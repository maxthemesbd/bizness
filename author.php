<?php
/**
 * Main index page
 */
get_header(); 
?>

<!--Page Header-->
<section class="page_header padding-top" <?php if(bizness_get_option('bizness_page_header_img') != ''){ ?>style="background: url('<?php echo esc_url( bizness_get_option('bizness_page_header_img') ); ?>');" <?php } else { ?>style="background: url('<?php echo esc_url( get_template_directory_uri() ).'/images/'; ?>page-tittle.jpg');"<?php } ?>>
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1>
          <?php the_author(); ?>
    	  </h1>
        <p><?php esc_html_e('Articles Posted by ', 'bizness'); ?></p>
        <div class="page_nav">
      	<?php if (function_exists('bizness_wordpress_breadcrumbs')) bizness_wordpress_breadcrumbs(); ?>
      </div>
      </div>
    </div>
  </div>
</section>

<!--BLOG SECTION-->
<section id="blog" class="padding">
  <div class="container">
    <h2 class="hidden"><?php esc_html_e('Our Blog', 'bizness'); ?></h2>
    <div class="row">
      <div class="col-md-9">       
        <?php
            $data_delay = 3; 
            if (have_posts()) :  while (have_posts()) : the_post();
            $bizness_global_post = bizness_get_global_post();
            $postid = $bizness_global_post->ID;
            $get_image = esc_url( wp_get_attachment_url( get_post_thumbnail_id($postid) ) );

            if( $data_delay > 5 ):
            $data_delay = 3;
            endif; 
         ?>        
        <article class="blog_item heading_space wow fadeInLeft" data-wow-delay="<?php echo esc_attr($data_delay); ?>00ms">
          <div class="row">

            <?php if ( has_post_thumbnail()){ ?>
            <div class="col-md-5 col-sm-5 heading_space">
              <div class="image"><img src="<?php echo esc_url( $get_image ); ?>" alt="" class="border_radius"></div>
            </div>
            <?php } ?>
            <?php 
              if ( has_post_thumbnail()){ $grid = "col-md-7 col-sm-7 heading_space"; } else { $grid = "col-md-12 col-sm-12 heading_space"; }
            ?>
            <div id="post-<?php the_ID(); ?>" <?php post_class( $grid ); ?>>
              <h3><?php the_title(); ?></h3>
              <ul class="comment margin10">
                <li><a href="<?php the_permalink(); ?>"><?php the_time( get_option('date_format') ); ?></a></li>
                <li><a href="<?php the_permalink(); ?>"><i class="icon-comment"></i> <?php echo get_comments_number( $postid ); ?></a></li>
              </ul>
              <?php if ( has_post_format( 'video' ) ) : ?>
                <?php the_content(); ?>
                <?php else: ?>
                <?php the_excerpt(); ?>
              <?php endif; ?>
              <a class=" btn_common btn_border margin10 border_radius" href="<?php the_permalink(); ?>"><?php esc_html_e('Read More', 'bizness'); ?></a>
            </div>

          </div>
        </article>
        <?php $data_delay++; endwhile; endif; ?> 

        <div class="pager_nav wow fadeIn" data-wow-delay="600ms">
          <?php bizness_custom_pagination(); ?>
        </div>
      </div>

		<!--Sidebar-->
			<?php get_sidebar(); ?>
		<!--Sidebar end-->		

    </div>
  </div>
</section>
<!--BLOG SECTION-->


<?php get_footer(); ?>