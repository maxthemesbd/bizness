<?php
/**
* Template Name: Page VC
*/
get_header(); 
$bizness_page_heading              = get_post_meta(get_the_ID(), 'bizness_page_heading', true);
$bizness_page_subheading           = get_post_meta(get_the_ID(), 'bizness_page_subheading', true);
$show_hide_page_header             = get_post_meta(get_the_ID(), 'show_hide_page_header', true);
$bizness_page_bg                   = get_post_meta(get_the_ID(), 'bizness_page_bg', true);
if($show_hide_page_header == "show"){
?>
<!--Page Header-->
<section class="page_header padding-top" <?php if( !empty($bizness_page_bg) ){ ?>style="background: url('<?php echo esc_url( $bizness_page_bg ); ?>');"<?php }else if(bizness_get_option('bizness_page_header_img') != ''){ ?>style="background: url('<?php echo esc_url( bizness_get_option('bizness_page_header_img') ); ?>');" <?php } else { ?>style="background: url('<?php echo esc_url( get_template_directory_uri() ).'/images/'; ?>page-tittle.jpg');"<?php } ?>>
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1>
          <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
            <?php ucwords(the_title()); ?>
          <?php endwhile; endif; ?>
        </h1>
        <p><?php bloginfo('description'); ?></p>
        <div class="page_nav">
          <?php if (function_exists('bizness_wordpress_breadcrumbs')) bizness_wordpress_breadcrumbs(); ?>
        </div>         
      </div>
    </div>
  </div>
</section>
<?php } ?>

<div class="container">
		<?php /* The loop */ ?>
      <?php while ( have_posts() ): the_post(); ?>

          <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

              <div class="entry-content">
                  <?php the_content(); ?>
                  <?php wp_link_pages(); ?>
              </div>

          </div>

      <?php endwhile; ?>
</div>
<?php get_footer(); ?>