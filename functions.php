<?php
function bizness_get_global_themedata() {
    global $bizness_theme_data;
    return $bizness_theme_data;
}
$bizness_themes_data = bizness_get_global_themedata();
$bizness_themes_data = wp_get_theme( get_stylesheet_directory() . '/style.css' );

/* -----------------------------------------------------------------------------
* Definations
* -------------------------------------------------------------------------- */

if( !defined('BIZNESS_ADMIN_PATH') )
	define( 'BIZNESS_ADMIN_PATH', get_template_directory() . '/framework/admin/' );
if( !defined('BIZNESS_INIT_PATH') )
	define( 'BIZNESS_INIT_PATH', get_template_directory() . '/framework/' );
if( !defined('BIZNESS_INCLUDE_PATH') )
	define( 'BIZNESS_INCLUDE_PATH', get_template_directory() . '/inc/' );
if( !defined('BIZNESS_LANGUAGE_PATH') )
	define( 'BIZNESS_LANGUAGE_PATH', get_template_directory() . '/languages/' );

require_once( BIZNESS_INIT_PATH . 'init.php' );

/*======================================*
*				Nav Walker
*=======================================*/

# Main Navigation.
require_once( get_template_directory()  . '/inc/menu/mobile-navwalker.php');
require_once( get_template_directory()  . '/inc/menu/megamenu-navwalker.php');
# End Nav Walker.