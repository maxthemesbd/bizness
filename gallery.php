<?php
/**
 * Template Name: Gallery
 */
get_header();
$bizness_page_heading              = get_post_meta(get_the_ID(), 'bizness_page_heading', true);
$bizness_page_subheading           = get_post_meta(get_the_ID(), 'bizness_page_subheading', true);
$bizness_page_bg                   = get_post_meta(get_the_ID(), 'bizness_page_bg', true);
$show_hide_page_header              = get_post_meta(get_the_ID(), 'show_hide_page_header', true);
if( $show_hide_page_header == "show" ){
?>
<!--Page Header-->
<section class="page_header padding-top" <?php if( !empty($bizness_page_bg)){ ?>style="background: url('<?php echo esc_url( $bizness_page_bg ); ?>');" <?php } else if(bizness_get_option('bizness_page_header_img') != ''){ ?>style="background: url('<?php echo esc_url( bizness_get_option('bizness_page_header_img') ); ?>');" <?php } else { ?>style="background: url('<?php echo esc_url( get_template_directory_uri() ).'/images/'; ?>page-tittle.jpg');"<?php } ?>>
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">

        <?php if( !empty($bizness_page_heading) ): ?>
          <h1><?php echo ucwords(strtolower(esc_attr($bizness_page_heading))); ?></h1>
        <?php else: ?>
          <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
          <h1><?php ucwords(the_title()); ?></h1>
          <?php endwhile; endif; ?>
        <?php endif; ?>

        <?php if( !empty($bizness_page_subheading) ): ?>
        <p><?php echo esc_attr( $bizness_page_subheading ); ?></p>
        <?php else: ?>        
        <p><?php echo bloginfo('name'); ?></p>
        <?php endif; ?> 

        <div class="page_nav">
          <?php if (function_exists('bizness_wordpress_breadcrumbs')) bizness_wordpress_breadcrumbs(); ?>
        </div>        
      </div>
    </div>
  </div>
</section>
<?php } ?>

<!-- Gallery -->
<section id="gallery" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-sm-5">
        <h2 class="heading heading_space"><span><?php esc_html_e('Our', 'bizness'); ?></span> <?php esc_html_e('Work', 'bizness'); ?><span class="divider-left"></span></h2>
      </div>
      <div class="col-sm-7">
        <div id="project-filter" class="cbp-l-filters">
          <div data-filter="*" class="cbp-filter-item-active cbp-filter-item"><?php esc_html_e('ALL IMAGES', 'bizness'); ?></div>
    	  	<?php 
      			$taxonomies = array('taxonomy' => 'gallery_cat');
      			$taxonomy_terms = get_terms( $taxonomies );
      			foreach ( $taxonomy_terms as $taxonomy_term ) :
        	?>          
          <div data-filter=".<?php echo esc_attr($taxonomy_term->term_id); ?>" class="cbp-filter-item"><?php echo esc_attr($taxonomy_term->name); ?></div>
          <?php endforeach; ?>	
        </div>
      </div>
    </div>
    
    <div id="projects" class="cbp">
		<?php 
			$args = array(
				'post_type' => 'bizness-gallery',
				'order'		=> 'desc',
				'posts_per_page' => -1	
			);
			$query  = new WP_Query( $args );
			if ($query->have_posts()) :  while ($query->have_posts()) : $query->the_post(); 
				$bizness_global_post 				= bizness_get_global_post();
				$postid 						      = $bizness_global_post->ID;
				$get_image 						    = esc_url( wp_get_attachment_url( get_post_thumbnail_id($postid) ) );
				$terms 							      = get_the_terms( $post->ID, 'gallery_cat' );
				if ($terms && ! is_wp_error($terms)) :
				    foreach ($terms as $term) {
				        $get_cate_id = $term->term_id;
				    }
				endif;			
		?>     	
    <div class="cbp-item <?php echo esc_attr($get_cate_id); ?>">
		<?php if( !empty(esc_url( $get_image )) ): ?>
		<img src="<?php echo esc_url( $get_image ); ?>" alt="">
		<?php endif; ?>      	
        <div class="overlay">
          <div class="centered text-center">
			<?php if( !empty(esc_url( $get_image )) ): ?>
            <a href="<?php echo esc_url( $get_image ); ?>" class="cbp-lightbox opens"> <i class=" icon-expand"></i></a> 
			<?php endif; ?>            	
          </div>
        </div>
      </div>
	  <?php endwhile; endif; ?>      
    </div>
  </div>
</section>
<!-- Gallery -->

<?php get_footer(); ?>