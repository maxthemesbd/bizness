<?php
/**
* Template Name: Blog Three
*/

get_header();
$bizness_page_heading              = get_post_meta(get_the_ID(), 'bizness_page_heading', true);
$bizness_page_subheading           = get_post_meta(get_the_ID(), 'bizness_page_subheading', true);
$bizness_page_bg                   = get_post_meta(get_the_ID(), 'bizness_page_bg', true);
$show_hide_page_header              = get_post_meta(get_the_ID(), 'show_hide_page_header', true);
if( $show_hide_page_header == "show" ){
?>
<!--Page Header-->
<section class="page_header padding-top" <?php if( !empty($bizness_page_bg)){ ?>style="background: url('<?php echo esc_url( $bizness_page_bg ); ?>');" <?php } else if(bizness_get_option('bizness_page_header_img') != ''){ ?>style="background: url('<?php echo esc_url( bizness_get_option('bizness_page_header_img') ); ?>');" <?php } else { ?>style="background: url('<?php echo esc_url( get_template_directory_uri() ).'/images/'; ?>page-tittle.jpg');"<?php } ?>>
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">

        <?php if( !empty($bizness_page_heading) ): ?>
          <h1><?php echo ucwords(strtolower(esc_attr($bizness_page_heading))); ?></h1>
        <?php else: ?>
          <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
          <h1><?php ucwords(the_title()); ?></h1>
          <?php endwhile; endif; ?>
        <?php endif; ?>

        <?php if( !empty($bizness_page_subheading) ): ?>
        <p><?php echo esc_attr( $bizness_page_subheading ); ?></p>
        <?php else: ?>        
        <p><?php echo bloginfo('name'); ?></p>
        <?php endif; ?> 

        <div class="page_nav">
          <?php if (function_exists('bizness_wordpress_breadcrumbs')) bizness_wordpress_breadcrumbs(); ?>
        </div>        
      </div>
    </div>
  </div>
</section>
<?php } ?>

<!--BLOG SECTION-->
<section id="blog" class="padding-top padding-bottom-half">
  <div class="container">
    <h2 class="hidden"><?php esc_html_e('Blog', 'bizness'); ?></h2>
    <div class="row">
        <?php   
          if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
          elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
          else { $paged = 1; }
          $args = array('order'=> 'DESC', 'paged' => $paged, 'orderby' => 'post_date' );
          query_posts($args);
        ?>        
        <?php
            $data_delay = 3;
            $allowed_html_array = array(
              'a' => array(),
              'span' => array(),
            );              
            if (have_posts()) :  while (have_posts()) : the_post();
            $bizness_global_post = bizness_get_global_post();
            $postid = $bizness_global_post->ID;
            $get_image = esc_url( wp_get_attachment_url( get_post_thumbnail_id($postid) ) );

            if( $data_delay > 12 ):
              $data_delay = 3;
            endif; 
        ?>  
      <div class="col-md-4 col-sm-6 content_wrap heading_space wow fadeIn" data-wow-delay="<?php echo esc_attr($data_delay); ?>00ms">
        <?php if ( has_post_thumbnail()): ?>
        <div class="image blog-three">
          <img src="<?php echo esc_url( $get_image ); ?>" alt="" class="img-responsive border_radius">
        </div>
        <?php endif; ?>
        <div class="news_box border_radius">
          <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
          <ul class="commment">
            <li><a href="<?php the_permalink(); ?>"><i class="icon-icons20"></i><?php the_time( get_option('date_format') ); ?></a></li>
            <li><a href="<?php the_permalink(); ?>"><i class="icon-comment"></i> <?php echo get_comments_number( $postid ); ?></a></li>
          </ul>
          <?php if ( has_post_format( 'video' ) ) : ?>
            <?php the_content(); ?>
            <?php else: ?>
            <p><?php echo bizness_excerpt_blog_three(56); ?></p>
          <?php endif; ?>
          <a href="<?php the_permalink(); ?>" class="readmore"><?php esc_html_e('Read More', 'bizness'); ?></a>
        </div>
      </div>
      <?php $data_delay++; endwhile; endif; ?> 

    </div>
  </div>
</section>
<!--BLOG SECTION-->


<?php get_footer(); ?>