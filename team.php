<?php
/**
 * Template Name: Team
 */
get_header();
$bizness_page_heading              = get_post_meta(get_the_ID(), 'bizness_page_heading', true);
$bizness_page_subheading           = get_post_meta(get_the_ID(), 'bizness_page_subheading', true);
$bizness_page_bg                   = get_post_meta(get_the_ID(), 'bizness_page_bg', true);
$show_hide_page_header              = get_post_meta(get_the_ID(), 'show_hide_page_header', true);
if( $show_hide_page_header == "show" ){
?>
<!--Page Header-->
<section class="page_header padding-top" <?php if( !empty($bizness_page_bg)){ ?>style="background: url('<?php echo esc_url( $bizness_page_bg ); ?>');" <?php } else if(bizness_get_option('bizness_page_header_img') != ''){ ?>style="background: url('<?php echo esc_url( bizness_get_option('bizness_page_header_img') ); ?>');" <?php } else { ?>style="background: url('<?php echo esc_url( get_template_directory_uri() ).'/images/'; ?>page-tittle.jpg');"<?php } ?>>
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">

        <?php if( !empty($bizness_page_heading) ): ?>
          <h1><?php echo ucwords(strtolower(esc_attr($bizness_page_heading))); ?></h1>
        <?php else: ?>
          <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
          <h1><?php ucwords(the_title()); ?></h1>
          <?php endwhile; endif; ?>
        <?php endif; ?>

        <?php if( !empty($bizness_page_subheading) ): ?>
        <p><?php echo esc_attr( $bizness_page_subheading ); ?></p>
        <?php else: ?>        
        <p><?php echo bloginfo('name'); ?></p>
        <?php endif; ?> 

        <div class="page_nav">
          <?php if (function_exists('bizness_wordpress_breadcrumbs')) bizness_wordpress_breadcrumbs(); ?>
        </div>        
      </div>
    </div>
  </div>
</section>
<?php } ?>


<!-- teams -->
<section id="teams" class="padding-bottom">
  <div class="container">
    <div class="row">
    <?php 
      $data_delay = 3;
      $allowed_html_array = array(
        'a' => array(),
        'span' => array(),
      );          
      if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
      elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
      else { $paged = 1; }
      $args = array('post_type'=> 'bizness-team','order'=> 'DESC', 'paged' => $paged, 'orderby' => 'post_date' );
      query_posts($args);    
    ?>
    <?php if (have_posts()) :  while (have_posts()) : the_post(); 
      $bizness_global_post       = bizness_get_global_post();
      $postid                 = $bizness_global_post->ID;
      $get_image              = esc_url( wp_get_attachment_url( get_post_thumbnail_id($postid) ) ); 

      $team_designation       = get_post_meta($postid, 'team_designation', true);
      $team_fb                = get_post_meta($postid, 'team_fb', true);
      $team_twitter           = get_post_meta($postid, 'team_twitter', true);
      $team_dribbble          = get_post_meta($postid, 'team_dribbble', true);       

      if( $data_delay > 11 ):
        $data_delay = 3;
      endif;                
    ?>      
      <div class="col-sm-6 col-md-4 equalheight">
        <div class="team margin_top wow fadeIn" data-wow-delay="<?php echo esc_attr($data_delay); ?>00ms">
          <?php if ( has_post_thumbnail() ): ?>          
          <div class="image bottom25">
            <img src="<?php echo esc_url($get_image); ?>" alt="" class="border_radius">
            <span class="post"><?php echo esc_attr($team_designation); ?></span>
          </div>
          <?php endif; ?>          
          <h3><?php the_title(); ?></h3>
          <p class="bottom20 margin10"><?php the_content(); ?></p>
          <ul class="social_icon black bottom5">
            <?php if( !empty($team_fb) ): ?>
            <li><a href="<?php echo esc_url($team_fb); ?>" class="facebook"><i class="fa fa-facebook"></i></a></li>
            <?php endif; ?>
            <?php if( !empty($team_twitter) ): ?>
            <li><a href="<?php echo esc_url($team_twitter); ?>" class="twitter"><i class="icon-twitter4"></i></a></li>
            <?php endif; ?>
            <?php if( !empty($team_dribbble) ): ?>
            <li><a href="<?php echo esc_url($team_dribbble); ?>" class="dribble"><i class="icon-dribbble5"></i></a></li>
            <?php endif; ?>
          </ul>
        </div>
      </div>
    <?php endwhile; endif; ?>
    </div>
  </div>
</section>
<!-- teams -->

<?php get_footer(); ?>