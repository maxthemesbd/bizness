<?php

/* -----------------------------------------------------------------------------
 * Helper Function
 * -------------------------------------------------------------------------- */
function bizness_get_option( $opt_name, $default = null ) {
	global $Redux_Options;
	return $Redux_Options->get( $opt_name, $default );
}

/* -----------------------------------------------------------------------------
* Load Custom Fields
* ------------------------------------------------------------------------------ */
require_once( BIZNESS_ADMIN_PATH . 'custom-fields/field_font_upload/field_upload.php' );
require_once( BIZNESS_ADMIN_PATH . 'custom-fields/field_typography.php' );
require_once( BIZNESS_ADMIN_PATH . 'custom-fields/field_section_info.php' );
require_once( BIZNESS_ADMIN_PATH . 'custom-fields/field_sidebar_select.php' );
require_once( BIZNESS_ADMIN_PATH . 'custom-fields/field_color_scheme.php' );
require_once( BIZNESS_ADMIN_PATH . 'custom-fields/field_slider.php' );
require_once( BIZNESS_ADMIN_PATH . 'custom-fields/field_slides.php' );

/* -----------------------------------------------------------------------------
* Initial Redux Framework
* -------------------------------------------------------------------------- */
if(!class_exists('Redux_Options')) {
	require_once( BIZNESS_ADMIN_PATH . 'options/defaults.php' );
}

function bizness_setup_framework_options() {
	$args = array();

	$args['std_show'] 				= true; // If true, it shows the std value
	$args['dev_mode_icon_class'] 	= 'icon-large';
	$args['share_icons']['twitter'] = array(
										'link' 		=> 'http://twitter.com/ghost1227',
										'title' 	=> esc_html('Follow me on Twitter', 'bizness'),
										'img' 		=> Redux_OPTIONS_URL . 'img/social/Twitter.png'
									);
	$args['share_icons']['linked_in'] = array(
		'link' 		=> 'http://www.linkedin.com/profile/view?id=52559281',
		'title' 	=> esc_html('Find me on LinkedIn', 'bizness'),
		'img' 		=> Redux_OPTIONS_URL . 'img/social/LinkedIn.png'
	);

	$args['import_icon_class'] 	= 'icon-large';
	$args['opt_name'] 			= 'bizness_theme_options';
	$args['menu_title'] 		= esc_html('Theme Options', 'bizness');
	$args['page_title'] 		= esc_html('Theme Options', 'bizness');
	$args['page_slug'] 			= 'redux_options';
	$args['page_type'] 			= 'menu';
	$args['page_parent'] 		= 'themes.php';

	
	$args['dev_mode_icon_type'] 	= 'iconfont';
	$args['import_icon_type'] 		= 'iconfont';
	$allowed_html_array = array(
    'p' => array(),
    'h4' => array(),
	'strong' => array(),
	'a' => array(),
	);
	$args['intro_text'] = wp_kses( __( '<h4>Theme Settings Information for BIZNESS Theme</h4>', 'bizness' ), $allowed_html_array );

	$sections = array();
	
	# section for General-logo, favicon, ratina, custom css
	$sections[] = array(
		'icon_type' 	=> 'iconfont',
		'icon' 			=> 'globe',
		'icon_class' 	=> 'icon-large',
		'title' 		=> esc_html('Top Bar', 'bizness'),
		'desc' 			=> esc_html('Top Bar Options', 'bizness'),
		'fields' 		=> array(

	        array(
	            'id'            => 'topbar-en',
	            'type'          => 'checkbox',
	            'title'         => esc_html('Topbar Enable Option', 'bizness'),
	            'sub_desc'      => esc_html('Topbar Enable', 'bizness'),
	            'switch'        => true,
	            'std'           => '1'
	        ),						
		    array(
				'id' 		=> 'bizness_slogan',
				'type' 		=> 'text',
				'title' 	=> esc_html('Website slogan', 'bizness'),
				'sub_desc' 	=> esc_html('Have any question?', 'bizness'),
			),	
		    array(
				'id' 		=> 'bizness_slogan_url',
				'type' 		=> 'text',
				'title' 	=> esc_html('Website slogan url', 'bizness'),
				'sub_desc' 	=> esc_html('', 'bizness'),
			),	
			array(
				'id' 		=> 'bizness_icon_one',
				'type' 		=> 'text',
				'title' 	=> esc_html('Icon One', 'bizness'),
				'sub_desc' 	=> esc_html('e.g( fa fa-mobile, icon-phone)', 'bizness'),
			),										    
			array(
				'id' 		=> 'bizness_icon_text_one',
				'type' 		=> 'text',
				'title' 	=> esc_html('Text One', 'bizness'),
				'sub_desc' 	=> esc_html('e.g (654) 332-112-222', 'bizness'),
			),
			array(
				'id' 		=> 'bizness_icon_two',
				'type' 		=> 'text',
				'title' 	=> esc_html('Icon Two', 'bizness'),
				'sub_desc' 	=> esc_html('e.g: fa fa-mobile, icon-phone', 'bizness'),
			),										    
			array(
				'id' 		=> 'bizness_icon_text_two',
				'type' 		=> 'text',
				'title' 	=> esc_html('Text Two', 'bizness'),
				'sub_desc' 	=> esc_html('e.g support@Bizness.com', 'bizness'),
			)																					
		)
	);

	# Header Settings
	$sections[] = array(
		'icon_type' 	=> 'iconfont',
		'icon' 			=> 'globe',
		'icon_class' 	=> 'icon-large',
		'title' 		=> esc_html('Header', 'bizness'),
		'desc' 			=> esc_html('All Logo & favicon', 'bizness'),
		'fields' 		=> array(

			array(
				'id' 			=> 'logo-text-en',
				'type' 			=> 'checkbox',
				'title' 		=> esc_html('Text Type Logo', 'bizness'),
				'sub_desc' 		=> esc_html('logo Text Enable', 'bizness'),
				'switch' 		=> true,
				'std' 			=> 0
			),
			array(
				'id' 			=> 'logo-text',
				'type' 			=> 'text',
				'title' 		=> esc_html('Logo', 'bizness'),
				'sub_desc' 		=> esc_html('Upload your custom site logo', 'bizness'),
				'default' 		=> 'BIZNESS', 
				'required' 		=> array('logo-text-en','equals','1')
			),
		    array(
				'id' 			=> 'logo',
				'type' 			=> 'upload',
				'title' 		=> esc_html('Logo', 'bizness'),
				'sub_desc' 		=> esc_html('Upload your custom site logo', 'bizness'),
				'default' 		=> array( 'url' => get_template_directory_uri() .'/images/logo.jpg' ), 
			),
			 array(
				'id' 			=> 'scroll-logo',
				'type' 			=> 'upload',
				'title' 		=> esc_html('Scroll-Logo', 'bizness'),
				'sub_desc' 		=> esc_html('Upload your custom site Scroll logo', 'bizness'),
				'default' 		=> array( 'url' => get_template_directory_uri() .'/images/logo.png' ), 
			),
			
			array(
				'id' 			=> 'bizness_favicon',
				'type' 			=> 'upload',
				'title' 		=> esc_html('Favicon', 'bizness'),
				'sub_desc' 		=> esc_html('Upload favicon image. Upload 64x64 favicon icon.', 'bizness'),
				'default' 		=> array( 'url' => get_template_directory_uri() .'/images/favicon.png' ), 
			),

			array(
				'id' 			=> 'bizness_page_header_img',
				'type' 			=> 'upload',
				'title' 		=> esc_html('Page Header Background Image', 'bizness'),
				'sub_desc' 		=> esc_html('Upload Header Background Image for General Pages and Posts', 'bizness'),
			),																				
			
			array( 
				"title" 		=> esc_html("Custom CSS", 'bizness'),
				"sub_desc" 		=> esc_html("Advanced CSS options, override existing code style.", 'bizness'),
				"id" 			=> "textarea_csscode",
				"std" 			=> "",
				"type" 			=> "textarea"
			),

		)
	);
	# End Header

	# Section for Shop icon url
	$sections[] = 
		array(
			'icon_type' 	=> 'iconfont',
			'icon' 			=> 'group',
			'icon_class' 	=> 'fa fa-shopping-basket',
			'title' 		=> esc_html('General Settings', 'bizness'),
			'desc' 			=> wp_kses( esc_html('', 'bizness'), $allowed_html_array ),
			'fields' 		=> array(
				array(
				    'id'       => 'shop_column',
				    'type'     => 'select',
				    'title'    => esc_html('Select Product Column', 'bizness'), 
				    'sub_desc' => esc_html('Select Product Column', 'bizness'),
				    'options'  => array(
								'3' => esc_html( 'Column 4', 'bizness' ),
								'4' => esc_html( 'Column 3', 'bizness' ),
						        '6' => esc_html( 'Column 2', 'bizness' ),
						    ),
				    'default'  => '3',
				),
				array(
                     'id'        => 'preset',
                     'type'      => 'image_select',
                     'compiler'  => true,
                     'title'     => esc_html('Preset Layout', 'bizness'),
                     'subtitle'  => esc_html('Select any preset', 'bizness'),
                     'options'   => array(
                         '1' => array('alt' => 'Preset 1',       'img' => Redux_OPTIONS_URL . 'img/presets/preset1.png'),
                         '2' => array('alt' => 'Preset 2',       'img' => Redux_OPTIONS_URL . 'img/presets/preset2.png'),
                         '3' => array('alt' => 'Preset 3',       'img' => Redux_OPTIONS_URL . 'img/presets/preset3.png'),
                         '4' => array('alt' => 'Preset 4',       'img' => Redux_OPTIONS_URL . 'img/presets/preset4.png'),
                         ),
                     'default'   => '1'
                ),													
		)
	);
	# End Shop


	//section for Social icon url
	$sections[] = 
		array(
			'icon_type' 	=> 'iconfont',
			'icon' 			=> 'group',
			'icon_class' 	=> 'icon-large',
			'title' 		=> esc_html('Social Media', 'bizness'),
			'desc' 			=> esc_html('This is options for setting up the social media of website. Do not forget to use http:// for any social urls.', 'bizness'),
			'fields' 		=> array(
                array(
					'id' 		=> 'social_facebook',
					'type' 		=> 'text',
					'title' 	=> esc_html('Facebook URL', 'bizness'), 
					'sub_desc' 	=> esc_html('The URL to your account page', 'bizness'),
					'std' 		=> '',
				),		
				array(
                    'id' 		=> 'social_twitter',
                    'type' 		=> 'text',
                    'title' 	=> esc_html('Twitter URL', 'bizness'),
                    'sub_desc' 	=> esc_html('The URL to your account page', 'bizness'),
                    'std' 		=> '',
                ),
				array(
					'id' 		=> 'social_googleplus',
					'type' 		=> 'text',
					'title' 	=> esc_html('GooglePlus URL', 'bizness'), 
					'sub_desc' 	=> esc_html('The URL to your account page', 'bizness'),
					'std' 		=> '',
				),

				array(
                    'id' 		=> 'social_linkedin',
                    'type' 		=> 'text',
                    'title' 	=> esc_html('Linkedin URL', 'bizness'),
                    'sub_desc' 	=> esc_html('The URL to your account page', 'bizness'),
                    'std' 		=> '',
                ),
				
				array(
					'id' 		=> 'social_flickr',
					'type' 		=> 'text',
					'title' 	=> esc_html('Flicker URL', 'bizness'), 
					'sub_desc' 	=> esc_html('The URL to your account page', 'bizness'),
					'std' 		=> '',
				),
				array(
					'id' 		=> 'social_youtube',
					'type' 		=> 'text',
					'title' 	=> esc_html('YouTube URL', 'bizness'), 
					'sub_desc' 	=> esc_html('The URL to your account page', 'bizness'),
					'std' 		=> '',
				),
				array(
					'id' 		=> 'social_instagram_url',
					'type' 		=> 'text',
					'title' 	=> esc_html('Instagram URL', 'bizness'), 
					'sub_desc' 	=> esc_html('The URL to your account page', 'bizness'),
					'std' 		=> '',
				),
		)
	);

						
		//section for Footer
					$sections[] = array(
							'icon_type' 	=> 'iconfont',
							'icon' 			=> 'columns',
							'icon_class' 	=> 'icon-large',
							'title' 		=>  esc_html('Footer/Contact', 'bizness'),
							'desc'  		=>  esc_html('This is options for Footer and Contact.', 'bizness'),
							'fields' 	=> array(
											
											array(
												'id' => 'bizness_footer_upper',
												'type' => 'checkbox',
												'title' => esc_html('Show/Hide Upper Footer', 'bizness'),
												'sub_desc' => esc_html('Show/Hide Upper Footer', 'bizness'),
												'switch' => true,
												'std' => '1' // 1 = checked | 0 = unchecked
											),
											
											array(
												'id' => 'bizness_footer_bottom',
												'type' => 'checkbox',
												'title' => esc_html('Show/Hide Bottom Footer', 'bizness'),
												'sub_desc' => esc_html('Show/Hide Bottom Footer', 'bizness'),
												'switch' => true,
												'std' => '1' // 1 = checked | 0 = unchecked
											),												
							
											array(
												'id' => 'copy_text',
												'type' => 'textarea',
												'title' => esc_html('Copyright Text', 'bizness'),
												'sub_desc' => esc_html('Enter your Copyright text here', 'bizness'),
												'std' => '',
											),
											
							)
						);

	$tabs = array();

		$theme_data = wp_get_theme();
		$item_uri = $theme_data->get('ThemeURI');
		$description = $theme_data->get('Description');
		$author = $theme_data->get('Author');
		$author_uri = $theme_data->get('AuthorURI');
		$version = $theme_data->get('Version');
		$tags = $theme_data->get('Tags');

	
	$item_info = '<div class="redux-opts-section-desc">';
	$item_info .= '<p class="redux-opts-item-data description item-uri">' . wp_kses( esc_html('<strong>Theme URL:</strong> ', 'bizness'), $allowed_html_array ) . '<a href="' . esc_url( $item_uri ) . '" target="_blank">' . $item_uri . '</a></p>';
	$item_info .= '<p class="redux-opts-item-data description item-author">' . wp_kses( esc_html('<strong>Author:</strong> ', 'bizness'), $allowed_html_array ) . ($author_uri ? '<a href="' . esc_url( $author_uri ) . '" target="_blank">' . $author . '</a>' : $author) . '</p>';
	$item_info .= '<p class="redux-opts-item-data description item-version">' . wp_kses( esc_html('<strong>Version:</strong> ', 'bizness'), $allowed_html_array ) . $version . '</p>';
	$item_info .= '<p class="redux-opts-item-data description item-description">' . $description . '</p>';
	$item_info .= '<p class="redux-opts-item-data description item-tags">' . wp_kses( esc_html('<strong>Tags:</strong> ', 'bizness'), $allowed_html_array ) . implode(', ', $tags) . '</p>';
	$item_info .= '</div>';

	global $Redux_Options;
	$Redux_Options = new Redux_Options($sections, $args, $tabs);
}
add_action('init', 'bizness_setup_framework_options', 0);