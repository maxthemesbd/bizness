<?php 

add_action('after_setup_theme', 'bizness_theme_setup');
function bizness_theme_setup(){
	load_theme_textdomain( 'bizness', esc_url( get_template_directory_uri() ).'/languages' );

	$locale = get_locale();
	$locale_file = BIZNESS_LANGUAGE_PATH . "$locale.php";
	if ( is_readable( $locale_file ) )
		require_once( $locale_file );
}

require_once( BIZNESS_ADMIN_PATH . 'options.php' ); // Load Options Framework
require_once( BIZNESS_INCLUDE_PATH . 'theme-functions.php' ); // Load Theme Functions
require_once( BIZNESS_INCLUDE_PATH . 'enqueue.php' ); // Enqueue JavaScripts & CSS
require_once( BIZNESS_INCLUDE_PATH . 'customcss.php' ); // Load Custom CSS
require_once( BIZNESS_INCLUDE_PATH . 'widgets.php' );

if ( class_exists( 'woocommerce' ) ) {
	require_once( BIZNESS_INCLUDE_PATH . 'custom-woocommerce-functions.php' ); // Load WooCommerce Functions
}

if(class_exists('WPBakeryVisualComposerAbstract')) {
	include_once( BIZNESS_INCLUDE_PATH . 'vc-shortcodes.php' ); // Load Visual Composer Customizations
}

if ( is_admin() ) {
	require_once( BIZNESS_INIT_PATH . 'plugins.php' );
}
