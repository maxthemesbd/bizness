<?php
function bizness_styles_custom() {
?>

<!-- Custom CSS Codes
========================================================= -->
<style id="custom-style">
	<?php echo esc_textarea( bizness_get_option('textarea_csscode') ); //Load Custom CSS from Theme-Options ?>

</style>

<?php }
add_action( 'wp_head', 'bizness_styles_custom', 100 );
?>