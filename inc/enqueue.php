<?php
function bizness_scripts_basic() {  

	/* ------------------------------------------------------------------------ */
	/* Enqueue Scripts */
	/* ------------------------------------------------------------------------ */
	/* Bootstrap */
	wp_enqueue_script('bootstrap-min-js', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '1.0.0', true);
	/* 3rd Parties */
	wp_enqueue_script('bootsnav-js', get_template_directory_uri() . '/js/bootsnav.js', array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('jquery-appear-js', get_template_directory_uri() . '/js/jquery.appear.js', array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('jquery-countTo-js', get_template_directory_uri() . '/js/jquery-countTo.js', array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('jquery-parallax-js', get_template_directory_uri() . '/js/jquery.parallax-1.1.3.js', array( 'jquery' ), '1.1.3', true);
	wp_enqueue_script('owl-carousel.min-js', get_template_directory_uri() . '/js/owl.carousel.min.js', array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('jquery-cubeportfolio-min-js', get_template_directory_uri() . '/js/jquery.cubeportfolio.min.js', array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('jquery-matchHeight-min-js', get_template_directory_uri() . '/js/jquery.matchHeight-min.js', array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('jquery-themepunch-tools-min-js', get_template_directory_uri() . '/js/jquery.themepunch.tools.min.js', array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('jquery.themepunch-revolution-min-js', get_template_directory_uri() . '/js/jquery.themepunch.revolution.min.js', array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('revolution-extension-layeranimation-min-js', get_template_directory_uri() . '/js/revolution.extension.layeranimation.min.js', array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('revolution-extension-navigation-min-js', get_template_directory_uri() . '/js/revolution.extension.navigation.min.js', array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('revolution-extension.parallax-min-js', get_template_directory_uri() . '/js/revolution.extension.parallax.min.js', array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('revolution-extension-slideanims-min-js', get_template_directory_uri() . '/js/revolution.extension.slideanims.min.js', array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('revolution-extension-video-min-js', get_template_directory_uri() . '/js/revolution.extension.video.min.js', array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('wow-min-js', get_template_directory_uri() . '/js/wow.min.js', array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('prettysocial-js', get_template_directory_uri() . '/js/jquery.prettySocial.min.js', array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('magnific-popup', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('styleswitcher-js', get_template_directory_uri() . '/js/styleswitcher.js', array( 'jquery' ), '1.0.0', false, false);
	wp_enqueue_script('styleselector-js', get_template_directory_uri() . '/js/styleselector.js', array( 'jquery' ), '1.0.0', false, false);

	/* Custom */
	wp_enqueue_script('functions-js', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '1.0.0', true);

	// Google Map
	wp_enqueue_script('maps-api', 'http://maps.google.com/maps/api/js?key=AIzaSyAOBKD6V47-g_3opmidcmFapb3kSNAR70U', array( 'jquery' ), '', true);
 	wp_enqueue_script('gmaps', get_template_directory_uri() . '/js/gmaps.min.js', array( 'jquery' ), '1.0.0', true);
	
	if ( is_singular() ) {
		wp_enqueue_script( 'comment-reply' );
	}
	wp_localize_script( 'bizness-main', 'prefix_object_name', array(
		'error_while_ajax_request' => esc_html( 'Error while ajax request', 'bizness' ),
		'thank_you_your_email_has_been_sent' => esc_html( 'Thank you, your email has been sent', 'bizness' ),
		'please_try_again' => esc_html( 'Please, fill in all the required fields correctly!', 'bizness' )
	) );
}
add_action( 'wp_enqueue_scripts', 'bizness_scripts_basic' ); 

function bizness_styles_basic()  {  
	
	/* ------------------------------------------------------------------------ */
	/* Enqueue Stylesheets */
	/* ------------------------------------------------------------------------ */
	/* Bootstrap */
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
	/* CSS STYLES */
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');
	wp_enqueue_style( 'Bizness-icons', get_template_directory_uri() . '/css/Bizness-icons.css');
	wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.min.css');
	wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/css/owl.carousel.css');
	wp_enqueue_style( 'owl-transitions', get_template_directory_uri() . '/css/owl.transitions.css');
	wp_enqueue_style( 'cubeportfolio', get_template_directory_uri() . '/css/cubeportfolio.min.css');
	wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/css/magnific-popup.css');
	wp_enqueue_style( 'settings', get_template_directory_uri() . '/css/settings.css');
	wp_enqueue_style( 'bootsnav', get_template_directory_uri() . '/css/bootsnav.css');
	wp_enqueue_style( 'color-switcher', get_template_directory_uri() . '/css/color-switcher.css');

	wp_enqueue_style( 'anora', get_template_directory_uri() . '/css/anora.css', true, true);
	global $wp_styles;
	$wp_styles->add_data( 'anora', 'title', 'Anora' );	
	wp_enqueue_style( 'fcgroup', get_template_directory_uri() . '/css/fcgroup.css', true, true);
	$wp_styles->add_data( 'fcgroup', 'title', 'Fcgroup' );
	wp_enqueue_style( 'cumen', get_template_directory_uri() . '/css/cumen.css', true, true);
	$wp_styles->add_data( 'cumen', 'title', 'Cumen' );	
	wp_enqueue_style( 'arient', get_template_directory_uri() . '/css/arient.css', true, true);
	$wp_styles->add_data( 'arient', 'title', 'Arient' );		

	wp_enqueue_style( 'loader', get_template_directory_uri() . '/css/loader.css');
	wp_enqueue_style( 'bizness-stylesheet', get_template_directory_uri() . '/style.css'); // Main Stylesheet
		
}  
add_action( 'wp_enqueue_scripts', 'bizness_styles_basic', 1 );
?>