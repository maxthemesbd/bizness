<?php
// **********************************************************************// 
// ! Supports post thumbnails and post formats
// **********************************************************************// 
add_theme_support( 'post-thumbnails' );
add_theme_support( 'post-formats', array( 'video' ) );
add_theme_support( 'custom-logo', array(
   'height'      => 35,
   'width'       => 200,
   'flex-width' => true,
) );
function bizness_the_custom_logo() {
   if ( function_exists( 'the_custom_logo' ) ) {
      the_custom_logo();
   }
}

function bizness_add_editor_styles() {
  add_editor_style( 'css/bootstrap.min.css' );
}
add_action( 'admin_init', 'bizness_add_editor_styles' );
  
// **********************************************************************// 
// ! Rss feeds, Custom Background and Title Tag theme supports
// **********************************************************************// 
add_theme_support( 'automatic-feed-links' );
add_theme_support('custom-background', array(
    'default-color' => '#ffffff',
));
add_theme_support( 'custom-header', array(
    'default-text-color' => '#000'
));
add_theme_support( 'title-tag' );


# ===================================== 
# ! Main Menu Register
# ======================================
function register_bizness_menu() {
    register_nav_menu('bizness_primary_menu', esc_html('Main Menu', 'bizness'));
    register_nav_menu('secondary_nav', esc_html('Secondary Menu', 'bizness'));
}
add_action( 'init', 'register_bizness_menu' );




// **********************************************************************// 
// ! Getting Theme Fonts
// **********************************************************************//
// ! Opensans Font
function bizness_fonts_opensans_url() {
$fonts_url = '';
 
/* Translators: If there are characters in your language that are not
* supported by Poppins, translate this to 'off'. Do not translate
* into your own language.
*/
$open_sans = _x( 'on', 'Open Sans font: on or off', 'bizness' );

$font_families = array();
 
if ( 'off' !== $open_sans ) {
$font_families[] = 'Open+Sans:400,700';
}
 
$query_args = array(
'family' => urlencode( implode( '|', $font_families ) ),
'subset' => urlencode( 'latin,latin-ext' ),
);
 
$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
 
return esc_url_raw( $fonts_url );
}

// ! Railway Font
function bizness_fonts_railway_url() {
$fonts_url = '';
 
/* Translators: If there are characters in your language that are not
* supported by Arimo, translate this to 'off'. Do not translate
* into your own language.
*/
$open_sans = _x( 'on', 'Railway font: on or off', 'bizness' );

$font_families = array();
 
if ( 'off' !== $open_sans ) {
$font_families[] = 'Railway:100,200,300,400,500,600,700';
}
 
$query_args = array(
'family' => urlencode( implode( '|', $font_families ) ),
'subset' => urlencode( 'latin,latin-ext' ),
);
 
$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
 
return esc_url_raw( $fonts_url );
}




// **********************************************************************// 
// ! Add breadcrumbs
// **********************************************************************//
function bizness_wordpress_breadcrumbs() {
 
  $allowed_html_array = array(
    'a' => array(),
    'span' => array(),
  );
  $delimiter = '<i class="fa fa-angle-double-right"></i>';
  $name = esc_html('Home', 'bizness'); //text for the 'Home' link
  $blog_name = esc_html('Blog', 'bizness'); //text for the 'Blog' link
  $currentBefore = '<span>';
  $currentAfter = '</span>';
 
  if ( !is_front_page() || is_paged() ) {
 
    echo '<ul class="bcrumbs">';
   
    global $post;
    $home = home_url();
    if ( is_home() ) {
     echo '<li><a href="' . esc_url($home) . '">' . $name . '</a> ' . $delimiter . ' '.$blog_name.'';
    } else {
     echo '<li><a href="' . esc_url($home) . '">' . $name . '</a> ' . $delimiter . '';
     
    }
 
    if ( is_category() ) {

      global $wp_query;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
      echo '<li>'. wp_kses( $currentBefore . esc_html( ' Archive by category &#39;', 'bizness' ), $allowed_html_array );
      echo ucwords(strtolower(single_cat_title()));
      echo wp_kses( ('&#39;' .$currentAfter . ''), $allowed_html_array ). '</li>';
 
    } elseif ( is_day() ) {

      echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . '';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo wp_kses( $currentBefore , $allowed_html_array );
      echo get_the_time('d');
      echo wp_kses( $currentAfter , $allowed_html_array ). '</li>';
 
    } elseif ( is_month() ) {

      echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo wp_kses( $currentBefore , $allowed_html_array );
      echo get_the_time('F');
      echo wp_kses( $currentAfter , $allowed_html_array ). '</li>';
 
    } elseif ( is_year() ) {

      echo '<li>';
      echo wp_kses( $currentBefore , $allowed_html_array );
      echo get_the_time('Y');
      echo wp_kses( $currentAfter , $allowed_html_array ). '</li>';
 
    } elseif ( is_single() ) {

      $cat = get_the_category(); $cat = (isset($cat[0]) ? $cat[0] : null);
      echo is_wp_error( $cat_parents = get_category_parents($cat, TRUE, ' ' . $delimiter . '') ) ? '' : $cat_parents;
      echo '<li>';
      echo '&nbsp;';
      echo wp_kses( $currentBefore , $allowed_html_array );
      echo ucwords(strtolower(get_the_title()));
      echo wp_kses( $currentAfter , $allowed_html_array ). '</li>';
 
    } elseif ( is_page() && !$post->post_parent ) {

      echo '<li>';
      echo wp_kses( $currentBefore , $allowed_html_array );
      echo ucwords(strtolower(get_the_title()));
      echo wp_kses( $currentAfter , $allowed_html_array ). '</li>';
 
    } elseif ( is_page() && $post->post_parent ) {

      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<li><a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a></li> ';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      foreach ($breadcrumbs as $crumb) echo wp_kses(( $crumb ), $allowed_html_array ) . ' ' . $delimiter . ' ';
        echo '<li>';
        echo wp_kses( $currentBefore , $allowed_html_array );
        echo ucwords(strtolower(get_the_title()));
        echo wp_kses( $currentAfter , $allowed_html_array ). '</li>';
 
    } elseif ( is_search() ) {

      echo wp_kses( $currentBefore , $allowed_html_array );
      echo esc_html( ' Search results for &#39;', 'bizness') . get_search_query() . '&#39;';
      echo wp_kses( $currentAfter , $allowed_html_array ). '</li>';
    
    } elseif ( is_tag() ) {

      echo '<li>';
      echo wp_kses( $currentBefore , $allowed_html_array );
      echo esc_html( 'Posts tagged &#39;', 'bizness');
      echo ucwords(strtolower(single_tag_title()));
      echo wp_kses( '&#39;' . $currentAfter , $allowed_html_array ). '</li>';
 
    } elseif ( is_author() ) {

      global $author;
      $userdata = get_userdata($author);
      echo '<li>';
      echo wp_kses( $currentBefore , $allowed_html_array );
      echo esc_html( 'Articles Posted by ', 'bizness') . $userdata->display_name;
      echo wp_kses( $currentAfter , $allowed_html_array ). '</li>';

    } elseif ( function_exists('is_shop') ) {

      if(is_shop()){
      echo '<li>' . esc_html( 'Shop', 'bizness') . '</li>';
      }
      
    } elseif ( is_404() ) {
      echo '<li>'. esc_attr($currentBefore) . esc_html( 'Error 404', 'bizness') . esc_attr($currentAfter). '</li>';
    }
 
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo esc_html('Page', 'bizness') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
 
    echo '</ul>';
 
  }
}
/// Breadcrumbs End ///

// **********************************************************************// 
// ! Register Sidebars
// **********************************************************************//
if ( ! function_exists( 'bizness_widgets_init' ) ) {
  function bizness_widgets_init() {

      register_sidebar( array(
      'name' => esc_html( 'Sidebar', 'bizness' ),
      'id' => 'bizness_sidebar',
      'before_widget' => '<div id="%1$s" class="widget heading_space %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h3 class="bottom20">',
      'after_title' => '</h3>',
    ) );    

    $before_widget = '<div id="%1$s" class="footer_panel bottom25 %2$s">';
    $after_widget = '</div>';
    $before_title = '<h3 class="heading bottom25">';
    $after_title = '<span class="divider-left"></span></h3>';
  
    register_sidebar( array(
      'name' => esc_html("Footer 1st Column Widget Area","bizness"),
      'id' => "footer_1c_sidebar",
      'before_widget' => $before_widget , 'after_widget' => $after_widget , 'before_title' => $before_title , 'after_title' => $after_title ,
    ));
    
    register_sidebar( array(
      'name' => esc_html("Footer 2nd Column Widget Area","bizness"),
      'id' => "footer_2c_sidebar",
      'before_widget' => $before_widget , 'after_widget' => $after_widget , 'before_title' => $before_title , 'after_title' => $after_title ,
    ));
    
    register_sidebar( array(
      'name' => esc_html("Footer 3rd Column Widget Area","bizness"),
      'id' => "footer_3c_sidebar",
      'before_widget' => $before_widget , 'after_widget' => $after_widget , 'before_title' => $before_title , 'after_title' => $after_title ,
    ));
    
  }
}
add_action( 'widgets_init', 'bizness_widgets_init' );


// **********************************************************************// 
// ! Get Global Variables
// **********************************************************************//
function bizness_get_global_post() {
    global $post;
    if ( 
        ! $post instanceof \WP_Post
    ) {
        return false;
    }
    return $post;
}

function bizness_get_global_wpquery() {
    global $wp_query;
    return $wp_query;
}

// **********************************************************************// 
// ! Custom Pagination
// **********************************************************************//
function bizness_custom_pagination() {
  global $wp_query;

  $big = 999999999; // need an unlikely integer
    
  echo paginate_links( array(
    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
    'format' => '?paged=%#%',
    'current' => max( 1, get_query_var('paged') ),
    'total' => $wp_query->max_num_pages,
    'show_all'     => false,
    'end_size'     => 1,
    'mid_size'     => 2,
    'prev_next'    => true,
    'prev_text'    => '<span aria-hidden="true">&laquo;',
    'next_text'    => '<span aria-hidden="true">Next &nbsp; <i class="fa fa-long-arrow-right"></i></span>',
    'type'         => 'list',
    'add_args'     => false,
    'add_fragment' => ''
  ) );
}

// **********************************************************************// 
// ! Excerpt Limit
// **********************************************************************//
function bizness_excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  } 
  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
  return $excerpt;
}

if ( ! function_exists( 'bizness_excerpt_length' ) ) {
  function bizness_excerpt_length( $length ) {
  return 47;
  }
}
add_filter( 'excerpt_length', 'bizness_excerpt_length', 999 );

// **********************************************************************// 
// ! Set Content Width
// **********************************************************************// 
if (!isset($content_width)) { $content_width = 750; }

add_filter('get_avatar','bizness_gravatar_class');

// **********************************************************************// 
// ! Adding Class to Gravatar image
// **********************************************************************//
function bizness_gravatar_class($class) {
    $class = str_replace("class='avatar", "class='avatar", $class);
    return $class;
}
// **********************************************************************// 
// ! Display Comments section
// **********************************************************************//
if ( ! function_exists( 'bizness_comment' ) ) {
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own bizness_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 */
  function bizness_comment( $comment, $args, $depth ) {
  $GLOBALS['comment'] = $comment;

  global $post;
  ?>
  <div <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
    <div class="profile_border">
      <div class="profile top20 bottom20">
      <?php
        $get_avatar = get_avatar( $comment, 90, '', '', array('class'=>'img-responsive') );
        if( !empty($get_avatar) ){?>        
      <div class="p_pic">
        <?php echo get_avatar( $comment, 90, '', '', array('class'=>'img-responsive') ); ?>
      </div>
      <?php } ?>
      <div class="profile_text">
         <h5><strong><?php comment_author(); ?></strong></h5>
        <ul class="comment">
           <li><a href="<?php get_the_permalink(); ?>"><?php echo mysql2date(get_option('date_format'), $comment->comment_date); ?> - <?php echo mysql2date(get_option('time_format'), comment_time()); ?></a></li>
        </ul>
        <?php comment_text()?>  
        <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'])), $comment, $post);?>       
      </div>  
    </div>
    </div>
  <!-- #comment-## -->

   <?php
   }
}

/*******************
Comment form styling
*******************/
if ( ! function_exists( 'bizness_modify_comment_fields' ) ) {
  function bizness_modify_comment_fields($fields) {

  $n_value = '';
  $e_value = '';
  $s_value = '';
  $fields['fields']  = '<div class="form-group"><input name="author" id="author" placeholder="Name" type="text" value="'.esc_attr($n_value).'" class="form-control" aria-required="true"/></div>';
  $fields['fields'] .= '<div class="form-group"><input type="email" id="email" placeholder="Email" name="email" value="'.esc_attr($e_value).'" class="form-control" aria-required="true" /></div>';
  return $fields;
  }
}

add_filter('comment_form_defaults', 'bizness_modify_comment_fields');//Name, Email and Website fields customization filter

if ( ! function_exists( 'bizness_comment_field' ) ) {
  function bizness_comment_field($arg) {
  
  $arg['comment_field'] = '
                <textarea placeholder="Your Comment" name="comment" id="comment"></textarea>
               ';    
  return $arg;
  }
}
add_filter('comment_form_defaults', 'bizness_comment_field');//Text area customization filter


/*=====================================================
*     WooCommerce Comments and Blog Comments Section
=======================================================*/
function bizness_comment_form_submit_button($button) {
  $button ='<button type="submit" class="btn_common yellow border_radius" id="[args:id_submit]" value="[args:label_submit]">'.esc_html("Submit", 'bizness').'</button>';
  return $button;
}
add_filter('comment_form_submit_button', 'bizness_comment_form_submit_button');
# Submit button End


function bizness_move_comment_field_to_bottom( $fields ) {
  $comment_field = $fields['comment'];
  unset( $fields['comment'] );
  $fields['comment'] = $comment_field;

  return $fields;
}
add_filter( 'comment_form_fields', 'bizness_move_comment_field_to_bottom' );//move the comment text field to the bottom

function bizness_change_reply_text( $link ) {
  return str_replace( 'Reply', 'Leave a Reply', $link );
}
add_filter( 'comment_reply_link', 'bizness_change_reply_text' );


/*******************
 REMOVE PARENTHESES AND ADD SPAN CLASS
*******************/
function bizness_categories_postcount_filter ($variable) {
  $variable = str_replace('(', '<span> ', $variable);
  $variable = str_replace(')', ' </span>', $variable);
  return $variable;
}
add_filter('wp_list_categories','bizness_categories_postcount_filter');

/*******************
 TAG CLOUD FONT SIZE
*******************/
function bizness_tag_cloud_font_unit($args) {
  $args['largest'] = 12; //largest tag
  $args['smallest'] = 12; //smallest tag
  $args['unit'] = 'px'; //tag font unit
  return $args;
}
add_filter( 'widget_tag_cloud_args', 'bizness_tag_cloud_font_unit' );

// Add css class in next & previous post link 
add_filter('next_post_link', 'bizness_post_link_class');
add_filter('previous_post_link', 'bizness_post_link_class');
function bizness_post_link_class($output) {
  $code = 'class="post_title"';
  return str_replace('<a href=', '<a '.$code.' href=', $output);
}

/*******************
 EXCERPT FOR BLOG THREE
*******************/
function bizness_excerpt_blog_three($excerpt_lenght){
      $excerpt = get_the_content();
      $excerpt = preg_replace(" ([.*?])",'',$excerpt);
      $excerpt = strip_shortcodes($excerpt);
      $excerpt = strip_tags($excerpt);
      $excerpt = substr($excerpt, 0, $excerpt_lenght);
      $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
      $excerpt = trim(preg_replace( '/s+/', ' ', $excerpt));
      return $excerpt;
} 

/*******************
 EXCERPT FOR SERVICE CPT
*******************/
function bizness_getexcerpt($excerpt_lenght){
      $excerpt = get_the_content();
      $excerpt = preg_replace(" ([.*?])",'',$excerpt);
      $excerpt = strip_shortcodes($excerpt);
      $excerpt = strip_tags($excerpt);
      $excerpt = substr($excerpt, 0, $excerpt_lenght);
      $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
      $excerpt = trim(preg_replace( '/s+/', ' ', $excerpt));
      return $excerpt;
} 


function enable_comments_custom_post_type() {
 add_post_type_support( 'bizness-service', 'comments' );
}
add_action( 'init', 'enable_comments_custom_post_type', 11 );