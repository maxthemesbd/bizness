<?php


# ================================
#       WooCommerce Support
#=================================
add_action( 'after_setup_theme', 'woocommerce_support' );
if ( ! function_exists( 'woocommerce_support' ) ) {
    function woocommerce_support() {
        add_theme_support( 'woocommerce' );
    }
}


# ================================
#       WooCommerce Css
#=================================
if ( class_exists( 'woocommerce' ) ) {
    add_action( 'get_footer', 'bizness_add_footer_styles' );
    function bizness_add_footer_styles() {
        wp_enqueue_style( 'bizness-shop-templates-css', get_template_directory_uri() . '/css/shop-templates.css');
    };
}


// **********************************************************************// 
// ! Customizing woocommerce breadcrumb
// **********************************************************************//
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
add_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
	function woocommerce_breadcrumb( $args = array() ) {
		$args = wp_parse_args( $args, apply_filters( 'woocommerce_breadcrumb_defaults', array(
				'delimiter'   => '',
				'wrap_before' => '<ul class="bcrumbs">',
				'wrap_after'  => '</ul>',
				'before'      => '<li>',
				'after'       => '</li>',
				'home'        => _x( 'Home', 'breadcrumb', 'denorious' )
			) ) );

		$breadcrumbs = new WC_Breadcrumb();

		if ( ! empty( $args['home'] ) ) {
			$breadcrumbs->add_crumb( $args['home'], apply_filters( 'woocommerce_breadcrumb_home_url', home_url() ) );
		}

		$args['breadcrumb'] = $breadcrumbs->generate();

		wc_get_template( 'global/breadcrumb.php', $args );
    }
