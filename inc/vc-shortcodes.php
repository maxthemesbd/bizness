<?php
// VC_row customization

vc_add_param("vc_row", array(
   "type" => "textfield",
   "class" => "",
   "heading" => esc_html("Want to have top padding?",'bizness'),
   "param_name" => "top_padding",
   "value" => "",
   "description" => esc_html("Input top padding in pixel (i.e 50px).", "bizness"),
));

vc_add_param("vc_row", array(
   "type" => "textfield",
   "class" => "",
   "heading" => esc_html("Want to have bottom padding?",'bizness'),
   "param_name" => "bottom_padding",
   "value" => "",
   "description" => esc_html("Input bottom padding in pixel (i.e 50px).", "bizness"),
));

vc_add_param("vc_row", array(
   "type" => "textfield",
   "class" => "",
   "heading" => esc_html("Want to have left padding?",'bizness'),
   "param_name" => "zleft_padding",
   "value" => "",
   "description" => esc_html("Input left padding in pixel (i.e 50px).", "bizness"),
));

vc_add_param("vc_row", array(
   "type" => "textfield",
   "class" => "",
   "heading" => esc_html("Want to have right padding?",'bizness'),
   "param_name" => "zright_padding",
   "value" => "",
   "description" => esc_html("Input right padding in pixel (i.e 50px).", "bizness"),
));

// VC_column_text customization

vc_add_param("vc_column_text", array(
   "type" => "colorpicker",
   "class" => "",
   "heading" => esc_html("Play text color", "bizness"),
   "param_name" => "txt_color",
   "value" => "",
   "description" => '',
));

/****************************************************
Removing default templates and adding custom template
****************************************************/
add_filter( 'vc_load_default_templates', 'my_custom_template_modify_array' ); // Remove default templates
function my_custom_template_modify_array( $data ) {
    return array(); // This will remove all default templates. Basically you should use native PHP functions to modify existing array and then return it.
}