<?php
/*************************
Start Bizness Search Widget
*************************/
class bizness_search_Widget extends WP_Widget {

       function __construct() {
    parent::__construct(
      'bizness_search_Widget', // Base ID
      esc_html('bizness -Bizness Search Widget', 'bizness'), // Name
      array( 'description' => esc_html( 'Search post, pages, custom post types and other stuff. Works in Sidebar.', 'bizness' ), ) // Args
    );
  }
/* Front-end display of widget. */
public function widget( $args, $instance ) {

      extract($args);

      ?>
      <div class="widget heading_space">
        <form method="get" role="search" action="<?php echo esc_url( home_url( '/' ) ); ?>" class="widget_search border-radius" id="bizness-sidebar-search">
          <div class="input-group">
            <input type="search"  id="s" name="s" class="form-control" placeholder="Search Here" required>
            <i class="input-group-addon icon-icons185 icon-submit" id="icon-submit"></i>
          </div>
        </form>        
      </div>

    <?php           
}

  function update( $new_instance, $old_instance ){

    $instance = $old_instance;
    return $instance;

  }

  function form($instance){
    $defaults = array( 
      'title'       => 'Search',
    );
    $instance = wp_parse_args( (array) $instance, $defaults );
  ?>
    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php esc_html_e('Widget Heading', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo esc_attr($instance['title']); ?>" style="width:100%;" />
    </p>
  <?php
  }

}//end of class

// register bizness_search_Widget widget
function bizness_register_search_Widget() {
  register_widget( 'bizness_search_Widget' );
}
add_action( 'widgets_init', 'bizness_register_search_Widget' );

/*************************
Start Bizness Tags
*************************/
class bizness_tags_Widget extends WP_Widget {

  function __construct() {
    parent::__construct(
      'bizness_tags_Widget', // Base ID
      esc_html('bizness -Tags Widget', 'bizness'), // Name
      array( 'description' => esc_html( 'Display top tags. Works in Sidebar.', 'bizness' ), ) // Args
    );
  }


      /*-------------------------------------------------------
       *        Front-end display of widget
       *-------------------------------------------------------*/


  public function widget( $args, $instance ) {

        extract($args);

        $title          = apply_filters('widget_title', $instance['title'] );

        //echo ;
        $output = '';

        $allowed_html_array = array(
        'div' => array(
          'class' => array(),
          'id' => array(),
          'style' => array()
        ),
        'h3' => array(
          'class' => array()
        ),
        'ul' => array(
          'class' => array()          
          ),
        'li' => array(),
        'a' => array(
          'class' => array(),
          'href' => array(),
          'style' => array()
        ));

        $output .='<div class="widget heading_space">
                    <h3 class="bottom20">'.$title.'</h3>
                      <ul class="tags">';
                      $tags = get_terms(array(
                              'taxonomy' => 'post_tag',
                              'orderby' => 'count',
                              'order' => 'DESC',
                              'number'  => 15,
                            ));
                  foreach( $tags as $tag):
              $output .= '<li><a href="' .get_tag_link ($tag->term_id). '">' . $tag->name . '</a></li>';      
                  endforeach;   
              $output .='                      
                      </ul>
                    </div>'; 

        echo wp_kses($output, $allowed_html_array);
  }

  function update( $new_instance, $old_instance ){

    $instance = $old_instance;
    $instance['title']            = strip_tags( $new_instance['title'] );
    return $instance;

  }


  function form($instance){
    $defaults = array( 
      'title'             => 'Top Tags',
    );
    $instance = wp_parse_args( (array) $instance, $defaults );
  ?>

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php esc_html_e('Heading', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo esc_attr($instance['title']); ?>" style="width:100%;" />
    </p>

  <?php
  }

}//end of class

// register bizness_tags_Widget widget
function bizness_register_tags_Widget() {
  register_widget( 'bizness_tags_Widget' );
}
add_action( 'widgets_init', 'bizness_register_tags_Widget' );


/*************************
Start Featured Courses
*************************/
class bizness_featured_courses_Widget extends WP_Widget {

  function __construct() {
    parent::__construct(
      'bizness_featured_courses_Widget', // Base ID
      esc_html('bizness -Featured Courses Widget', 'bizness'), // Name
      array( 'description' => esc_html( 'Display courses. Works in Sidebar.', 'bizness' ), ) // Args
    );
  }


      /*-------------------------------------------------------
       *        Front-end display of widget
       *-------------------------------------------------------*/


  public function widget( $args, $instance ) {

        extract($args);

        $title                                  = apply_filters('widget_title', $instance['title'] );
        $icon_img_url1                          = esc_attr($instance['icon_img_url1']);
        $course_title1                          = esc_attr($instance['course_title1']);
        $course1_type                           = esc_attr($instance['course1_type']);
        $course1_url                            = esc_attr($instance['course1_url']);
        $course_teacher1                        = esc_attr($instance['course_teacher1']);

        $icon_img_url2                          = esc_attr($instance['icon_img_url2']);
        $course_title2                          = esc_attr($instance['course_title2']);
        $course2_type                           = esc_attr($instance['course2_type']);
        $course2_url                            = esc_attr($instance['course2_url']);
        $course_teacher2                        = esc_attr($instance['course_teacher2']);           

        $icon_img_url3                          = esc_attr($instance['icon_img_url3']);
        $course_title3                          = esc_attr($instance['course_title3']);
        $course3_type                           = esc_attr($instance['course3_type']);
        $course3_url                            = esc_attr($instance['course3_url']);
        $course_teacher3                        = esc_attr($instance['course_teacher3']);  

        $allowed_html_array = array(
        'div' => array(
          'class' => array(),
          'id' => array(),
          'style' => array()
        ),
        'h3' => array(
          'class' => array()
        ),        
        'a' => array(
          'class' => array(),
          'href' => array()
        ),
        'img' => array(
          'class' => array(),
          'src' => array(),
          'alt' => array()
        ),
        'h5' => array(
          'class' => array()
        ),
        'span' => array(
          'class' => array()
        )
        );


        //echo ;
        $output = '';
        $output .='<div class="widget heading_space">';
        $output .='<h3 class="bottom20">'.$title.'</h3>';
        if( !empty($course_title1) && !empty($icon_img_url1) && !empty($course_teacher1) ){
        $output .='
            <div class="media">
              <a class="media-left" href="'.esc_url($course1_url).'"><img src="'.esc_url($icon_img_url1).'" alt=""></a>
              <div class="media-body">
                <h5 class="bottom5">'.esc_attr($course_title1).'</h5>
                <a href="'.esc_url($course1_url).'" class="btn-primary border_radius bottom5">'.esc_attr($course1_type).'</a>
                <span class="name">'.esc_attr($course_teacher1).'</span>
              </div>
            </div>';
          }
          
          if( !empty($course_title2) && !empty($icon_img_url2) && !empty($course_teacher2) ){ 
          $output .='  
            <div class="media">
              <a class="media-left" href="'.esc_url($course2_url).'"><img src="'.esc_url($icon_img_url2).'" alt=""></a>
              <div class="media-body">
                <h5 class="bottom5">'.esc_attr($course_title2).'</h5>
                <a href="'.esc_url($course2_url).'" class="btn-primary border_radius bottom5">'.esc_attr($course2_type).'</a>
                <span class="name">'.esc_attr($course_teacher2).'</span>
              </div>
            </div>';
          }

          if( !empty($course_title3) && !empty($icon_img_url3) && !empty($course_teacher3) ){ 
          $output .='      
            <div class="media">
              <a class="media-left" href="'.esc_url($course3_url).'"><img src="'.esc_url($icon_img_url3).'" alt=""></a>
              <div class="media-body">
                <h5 class="bottom5">'.esc_attr($course_title3).'</h5>
                <a href="'.esc_url($course3_url).'" class="btn-primary border_radius bottom5">'.esc_attr($course3_type).'</a>
                <span class="name">'.esc_attr($course_teacher3).'</span>
              </div>
            </div>
        ';
        }

        $output .='</div>'; 

        echo wp_kses($output, $allowed_html_array);
  }

  function update( $new_instance, $old_instance ){

    $instance = $old_instance;
    $instance['title']                              = strip_tags( $new_instance['title'] );
    $instance['icon_img_url1']                      = strip_tags( $new_instance['icon_img_url1'] );
    $instance['course_title1']                      = strip_tags( $new_instance['course_title1'] );
    $instance['course1_type']                      = strip_tags( $new_instance['course1_type'] );
    $instance['course1_url']                        = strip_tags( $new_instance['course1_url'] );
    $instance['course_teacher1']                    = strip_tags( $new_instance['course_teacher1'] );

    $instance['icon_img_url2']                      = strip_tags( $new_instance['icon_img_url2'] );
    $instance['course_title2']                      = strip_tags( $new_instance['course_title2'] );
    $instance['course2_type']                       = strip_tags( $new_instance['course2_type'] );
    $instance['course2_url']                        = strip_tags( $new_instance['course2_url'] );
    $instance['course_teacher2']                    = strip_tags( $new_instance['course_teacher2'] );   

    $instance['icon_img_url3']                      = strip_tags( $new_instance['icon_img_url3'] );
    $instance['course_title3']                      = strip_tags( $new_instance['course_title3'] );
    $instance['course3_type']                       = strip_tags( $new_instance['course3_type'] );
    $instance['course3_url']                        = strip_tags( $new_instance['course3_url'] );
    $instance['course_teacher3']                    = strip_tags( $new_instance['course_teacher3'] );  

    return $instance;

  }


  function form($instance){
    $defaults = array( 
      'title'                     => 'Featured Course',
      'icon_img_url1'             => '',
      'course_title1'             => '',
      'course1_type'             => '',
      'course1_url'               => '',
      'course_teacher1'           => '',

      'icon_img_url2'             => '',
      'course_title2'             => '',
      'course2_type'             => '',
      'course2_url'               => '',
      'course_teacher2'           => '', 

      'icon_img_url3'             => '',
      'course_title3'             => '',
      'course3_type'             => '',
      'course3_url'               => '',
      'course_teacher3'           => '',      

    );
    $instance = wp_parse_args( (array) $instance, $defaults );
  ?>

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php esc_html_e('Heading', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo esc_attr($instance['title']); ?>" style="width:100%;" />
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'icon_img_url1' )); ?>"><?php esc_html_e('Icon Img 1 URL', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'icon_img_url1' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'icon_img_url1' )); ?>" value="<?php echo esc_attr($instance['icon_img_url1']); ?>" style="width:100%;" />
      <label for="<?php echo esc_attr($this->get_field_id( 'course_title1' )); ?>"><?php esc_html_e('Course 1 Title', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'course_title1' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'course_title1' )); ?>" value="<?php echo esc_attr($instance['course_title1']); ?>" style="width:100%;" />

      <label for="<?php echo esc_attr($this->get_field_id( 'course1_type' )); ?>"><?php esc_html_e('Course Type', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'course1_type' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'course1_type' )); ?>" value="<?php echo esc_attr($instance['course1_type']); ?>" style="width:100%;" />
      
      <label for="<?php echo esc_attr($this->get_field_id( 'course1_url' )); ?>"><?php esc_html_e('Course Details URL', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'course1_url' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'course1_url' )); ?>" value="<?php echo esc_attr($instance['course1_url']); ?>" style="width:100%;" />      
      
      <label for="<?php echo esc_attr($this->get_field_id( 'course_teacher1' )); ?>"><?php esc_html_e('Course Teacher 1', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'course_teacher1' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'course_teacher1' )); ?>" value="<?php echo esc_attr($instance['course_teacher1']); ?>" style="width:100%;" />
    </p>

    <hr>
    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'icon_img_url2' )); ?>"><?php esc_html_e('Icon Img 2 URL', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'icon_img_url2' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'icon_img_url2' )); ?>" value="<?php echo esc_attr($instance['icon_img_url2']); ?>" style="width:100%;" />
      <label for="<?php echo esc_attr($this->get_field_id( 'course_title2' )); ?>"><?php esc_html_e('Course 2 Title', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'course_title2' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'course_title2' )); ?>" value="<?php echo esc_attr($instance['course_title2']); ?>" style="width:100%;" />

      <label for="<?php echo esc_attr($this->get_field_id( 'course2_type' )); ?>"><?php esc_html_e('Course Type', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'course2_type' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'course2_type' )); ?>" value="<?php echo esc_attr($instance['course2_type']); ?>" style="width:100%;" />
      
      <label for="<?php echo esc_attr($this->get_field_id( 'course2_url' )); ?>"><?php esc_html_e('Course Details URL', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'course2_url' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'course2_url' )); ?>" value="<?php echo esc_attr($instance['course2_url']); ?>" style="width:100%;" />      
      
      <label for="<?php echo esc_attr($this->get_field_id( 'course_teacher2' )); ?>"><?php esc_html_e('Course Teacher 2', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'course_teacher2' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'course_teacher2' )); ?>" value="<?php echo esc_attr($instance['course_teacher2']); ?>" style="width:100%;" />
    </p>

    <hr>
    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'icon_img_url3' )); ?>"><?php esc_html_e('Icon Img 3 URL', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'icon_img_url3' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'icon_img_url3' )); ?>" value="<?php echo esc_attr($instance['icon_img_url3']); ?>" style="width:100%;" />
      <label for="<?php echo esc_attr($this->get_field_id( 'course_title3' )); ?>"><?php esc_html_e('Course 3 Title', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'course_title3' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'course_title3' )); ?>" value="<?php echo esc_attr($instance['course_title3']); ?>" style="width:100%;" />

      <label for="<?php echo esc_attr($this->get_field_id( 'course3_type' )); ?>"><?php esc_html_e('Course Type', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'course3_type' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'course3_type' )); ?>" value="<?php echo esc_attr($instance['course3_type']); ?>" style="width:100%;" />
      
      <label for="<?php echo esc_attr($this->get_field_id( 'course3_url' )); ?>"><?php esc_html_e('Course Details URL', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'course3_url' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'course3_url' )); ?>" value="<?php echo esc_attr($instance['course3_url']); ?>" style="width:100%;" />      
      
      <label for="<?php echo esc_attr($this->get_field_id( 'course_teacher3' )); ?>"><?php esc_html_e('Course Teacher 3', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'course_teacher3' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'course_teacher3' )); ?>" value="<?php echo esc_attr($instance['course_teacher3']); ?>" style="width:100%;" />
    </p>         

  <?php
  }

}//end of class

// register bizness_tags_Widget widget
function bizness_register_featured_courses_Widget() {
  register_widget( 'bizness_featured_courses_Widget' );
}
add_action( 'widgets_init', 'bizness_register_featured_courses_Widget' );


/*************************
Start Bizness About
*************************/
class bizness_about_Widget extends WP_Widget {

  function __construct() {
    parent::__construct(
      'bizness_about_Widget', // Base ID
      esc_html('bizness -About Widget', 'bizness'), // Name
      array( 'description' => esc_html( 'Display About. Works in Footer.', 'bizness' ), ) // Args
    );
  }


      /*-------------------------------------------------------
       *        Front-end display of widget
       *-------------------------------------------------------*/


  public function widget( $args, $instance ) {

        extract($args);

        $title                = apply_filters('widget_title', $instance['title'] );
        $logo_img_url         = esc_attr($instance['logo_img_url']);
        $bizness_about           = esc_attr($instance['bizness_about']);
        $bizness_fb              = esc_attr($instance['bizness_fb']);
        $bizness_twitter         = esc_attr($instance['bizness_twitter']);
        $bizness_googleplus      = esc_attr($instance['bizness_googleplus']);
        $bizness_instagram       = esc_attr($instance['bizness_instagram']);
        $bizness_youtube         = esc_attr($instance['bizness_youtube']);
        $bizness_linkedin        = esc_attr($instance['bizness_linkedin']);
        $bizness_dribbble        = esc_attr($instance['bizness_dribbble']);
        $bizness_vimeo           = esc_attr($instance['bizness_vimeo']);

        $allowed_html_array = array(
        'h3' => array(
          'class' => array()
        ),
        'span' => array(
          'class' => array()
        ),
        'a' => array(
          'class' => array(),
          'href' => array()
        ),
        'img' => array(
          'class' => array(),
          'src' => array(),
          'alt' => array()
        ),        
        'p' => array(
          'class' => array()
        ),                                  
        'ul' => array(
          'class' => array()
          ),
        'li' => array(),
        'i' => array(
          'class' => array()
        ));        

        //echo ;
        $output = '';
        $output .= '<h3 class="heading bottom25">'.$title.'<span class="divider-left"></span></h3>';
        if( !empty($logo_img_url) ):
        $output .= '<a href="'.esc_url(home_url('/')).'" class="footer_logo bottom25"><img src="'.esc_url($logo_img_url).'" alt=""></a>';
        endif;
        $output .= '<p>'.esc_attr($bizness_about).'</p>';
        $output .='<ul class="social_icon top25">';

          if( !empty($bizness_fb) ):
          $output .='<li><a href="'.esc_url($bizness_fb).'" class="facebook"><i class="fa fa-facebook"></i></a></li>';
          endif;
          if( !empty($bizness_twitter) ):
          $output .='<li><a href="'.esc_url($bizness_twitter).'" class="twitter"><i class="icon-twitter4"></i></a></li>';
          endif;
          if( !empty($bizness_googleplus) ):
          $output .='<li><a href="'.esc_url($bizness_googleplus).'" class="googleplus"><i class="icon-googleplus"></i></a></li>';
          endif;
          if( !empty($bizness_instagram) ):
          $output .='<li><a href="'.esc_url($bizness_instagram).'" class="instagram"><i class="icon-instagram"></i></a></li>';
          endif;
          if( !empty($bizness_youtube) ):
          $output .='<li><a href="'.esc_url($bizness_youtube).'" class="youtube"><i class="icon-youtube5"></i></a></li>';
          endif;
          if( !empty($bizness_linkedin) ):
          $output .='<li><a href="'.esc_url($bizness_linkedin).'" class="linkedin"><i class="icon-linkedin4"></i></a></li>';
          endif;
          if( !empty($bizness_dribbble) ):
          $output .='<li><a href="'.esc_url($bizness_dribbble).'" class="dribbble"><i class="icon-dribbble5"></i></a></li>';
          endif;
          if( !empty($bizness_vimeo) ):
          $output .='<li><a href="'.esc_url($bizness_vimeo).'" class="vimeo"><i class="icon-vimeo4"></i></a></li>';
          endif;

        $output .='</ul>';


        echo wp_kses($output, $allowed_html_array);
  }

  function update( $new_instance, $old_instance ){

    $instance = $old_instance;
    $instance['title']                    = strip_tags( $new_instance['title'] );
    $instance['logo_img_url']             = strip_tags( $new_instance['logo_img_url'] );
    $instance['bizness_about']               = strip_tags( $new_instance['bizness_about'] );
    $instance['bizness_fb']                  = strip_tags( $new_instance['bizness_fb'] );
    $instance['bizness_twitter']             = strip_tags( $new_instance['bizness_twitter'] );
    $instance['bizness_googleplus']          = strip_tags( $new_instance['bizness_googleplus'] );
    $instance['bizness_instagram']           = strip_tags( $new_instance['bizness_instagram'] );
    $instance['bizness_youtube']             = strip_tags( $new_instance['bizness_youtube'] );
    $instance['bizness_linkedin']            = strip_tags( $new_instance['bizness_linkedin'] );
    $instance['bizness_dribbble']            = strip_tags( $new_instance['bizness_dribbble'] );
    $instance['bizness_vimeo']               = strip_tags( $new_instance['bizness_vimeo'] );
    return $instance;

  }


  function form($instance){
    $defaults = array( 
      'title'                       => 'About',
      'logo_img_url'                => '',
      'bizness_about'                  => '',
      'bizness_fb'                     => '',
      'bizness_twitter'                => '',
      'bizness_googleplus'             => '',
      'bizness_instagram'              => '',
      'bizness_youtube'                => '',
      'bizness_linkedin'               => '',
      'bizness_dribbble'               => '',
      'bizness_vimeo'                  => '',

    );
    $instance = wp_parse_args( (array) $instance, $defaults );
  ?>

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php esc_html_e('Heading', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo esc_attr($instance['title']); ?>" style="width:100%;" />
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'logo_img_url' )); ?>"><?php esc_html_e('Logo URL', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'logo_img_url' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'logo_img_url' )); ?>" value="<?php echo esc_attr($instance['logo_img_url']); ?>" style="width:100%;" />
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_about' )); ?>"><?php esc_html_e('About Text', 'bizness'); ?></label>
      <textarea id="<?php echo esc_attr($this->get_field_id( 'bizness_about' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_about' )); ?>" style="width:100%;"><?php echo esc_attr($instance['bizness_about']); ?></textarea>
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_fb' )); ?>"><?php esc_html_e('Facebook URL', 'bizness'); ?></label>
      <input type="text" id="<?php echo esc_attr($this->get_field_id( 'bizness_fb' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_fb' )); ?>" value="<?php echo esc_attr($instance['bizness_fb']); ?>" style="width:100%;"/>
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_twitter' )); ?>"><?php esc_html_e('Twitter URL', 'bizness'); ?></label>
      <input type="text" id="<?php echo esc_attr($this->get_field_id( 'bizness_twitter' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_twitter' )); ?>" value="<?php echo esc_attr($instance['bizness_twitter']); ?>" style="width:100%;"/>
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_googleplus' )); ?>"><?php esc_html_e('Google Plus URL', 'bizness'); ?></label>
      <input type="text" id="<?php echo esc_attr($this->get_field_id( 'bizness_googleplus' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_googleplus' )); ?>" value="<?php echo esc_attr($instance['bizness_googleplus']); ?>" style="width:100%;"/>
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_instagram' )); ?>"><?php esc_html_e('Instagram URL', 'bizness'); ?></label>
      <input type="text" id="<?php echo esc_attr($this->get_field_id( 'bizness_instagram' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_instagram' )); ?>" value="<?php echo esc_attr($instance['bizness_instagram']); ?>" style="width:100%;" />
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_youtube' )); ?>"><?php esc_html_e('Youtube URL', 'bizness'); ?></label>
      <input type="text" id="<?php echo esc_attr($this->get_field_id( 'bizness_youtube' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_youtube' )); ?>" value="<?php echo esc_attr($instance['bizness_youtube']); ?>"style="width:100%;" />
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_linkedin' )); ?>"><?php esc_html_e('Linkedin URL', 'bizness'); ?></label>
      <input type="text" id="<?php echo esc_attr($this->get_field_id( 'bizness_linkedin' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_linkedin' )); ?>" value="<?php echo esc_attr($instance['bizness_linkedin']); ?>" style="width:100%;" />
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_dribbble' )); ?>"><?php esc_html_e('Dribble URL', 'bizness'); ?></label>
      <input type="text" id="<?php echo esc_attr($this->get_field_id( 'bizness_dribbble' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_dribbble' )); ?>" value="<?php echo esc_attr($instance['bizness_dribbble']); ?>" style="width:100%;">
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_vimeo' )); ?>"><?php esc_html_e('Vimeo URL', 'bizness'); ?></label>
      <input type="text" id="<?php echo esc_attr($this->get_field_id( 'bizness_vimeo' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_vimeo' )); ?>" value="<?php echo esc_attr($instance['bizness_vimeo']); ?>" style="width:100%;">
    </p>

  <?php
  }

}//end of class

// register bizness_about_Widget widget
function bizness_register_about_Widget() {
  register_widget( 'bizness_about_Widget' );
}
add_action( 'widgets_init', 'bizness_register_about_Widget' );


/*************************
Start Bizness Quick Links
*************************/
class bizness_quick_links_Widget extends WP_Widget {

  function __construct() {
    parent::__construct(
      'bizness_quick_links_Widget', // Base ID
      esc_html('bizness -Quick Links Widget', 'bizness'), // Name
      array( 'description' => esc_html( 'Display Quick Links. Works in Footer.', 'bizness' ), ) // Args
    );
  }


      /*-------------------------------------------------------
       *        Front-end display of widget
       *-------------------------------------------------------*/


  public function widget( $args, $instance ) {

        extract($args);

        $title                          = apply_filters('widget_title', $instance['title'] );
        $bizness_text1                     = esc_attr($instance['bizness_text1']);
        $bizness_link1                     = esc_attr($instance['bizness_link1']);

        $bizness_text2                     = esc_attr($instance['bizness_text2']);
        $bizness_link2                     = esc_attr($instance['bizness_link2']);
        
        $bizness_text3                     = esc_attr($instance['bizness_text3']);
        $bizness_link3                     = esc_attr($instance['bizness_link3']);

        $bizness_text4                     = esc_attr($instance['bizness_text4']);
        $bizness_link4                     = esc_attr($instance['bizness_link4']);

        $bizness_text5                     = esc_attr($instance['bizness_text5']);
        $bizness_link5                     = esc_attr($instance['bizness_link5']);
        
        $bizness_text6                     = esc_attr($instance['bizness_text6']);
        $bizness_link6                     = esc_attr($instance['bizness_link6']);
        
        $bizness_text7                     = esc_attr($instance['bizness_text7']);
        $bizness_link7                     = esc_attr($instance['bizness_link7']);

        $bizness_text8                     = esc_attr($instance['bizness_text8']);
        $bizness_link8                     = esc_attr($instance['bizness_link8']);
        
        $bizness_text9                     = esc_attr($instance['bizness_text9']);
        $bizness_link9                     = esc_attr($instance['bizness_link9']);

        $bizness_text10                    = esc_attr($instance['bizness_text10']);
        $bizness_link10                    = esc_attr($instance['bizness_link10']);

        $allowed_html_array = array(
        'h3' => array(
          'class' => array()
        ),
        'span' => array(
          'class' => array()
        ),
        'ul' => array(
          'class' => array()
          ), 
        'li' => array(),               
        'a' => array(
          'class' => array(),
          'href' => array()
        ),
        'i' => array(
          'class' => array()
        ));        

        //echo ;
        $output = '';
        $output .= '<h3 class="heading bottom25">'.$title.'<span class="divider-left"></span></h3>';
        $output .='<ul class="links">';
          if( !empty($bizness_text1) ):
          $output .='<li><a href="'.esc_url($bizness_link1).'"><i class="icon-chevron-small-right"></i>'.esc_attr($bizness_text1).'</a></li>';
          endif;
          if( !empty($bizness_text2) ):
          $output .='<li><a href="'.esc_url($bizness_link2).'"><i class="icon-chevron-small-right"></i>'.esc_attr($bizness_text2).'</a></li>';
          endif;
          if( !empty($bizness_text3) ):
          $output .='<li><a href="'.esc_url($bizness_link3).'"><i class="icon-chevron-small-right"></i>'.esc_attr($bizness_text3).'</a></li>';
          endif;
          if( !empty($bizness_text4) ):
          $output .='<li><a href="'.esc_url($bizness_link4).'"><i class="icon-chevron-small-right"></i>'.esc_attr($bizness_text4).'</a></li>';
          endif;
          if( !empty($bizness_text5) ):
          $output .='<li><a href="'.esc_url($bizness_link5).'"><i class="icon-chevron-small-right"></i>'.esc_attr($bizness_text5).'</a></li>';
          endif;

          if( !empty($bizness_text6) ):
          $output .='<li><a href="'.esc_url($bizness_link6).'"><i class="icon-chevron-small-right"></i>'.esc_attr($bizness_text6).'</a></li>';
          endif;
          if( !empty($bizness_text7) ):
          $output .='<li><a href="'.esc_url($bizness_link7).'"><i class="icon-chevron-small-right"></i>'.esc_attr($bizness_text7).'</a></li>';
          endif;
          if( !empty($bizness_text8) ):
          $output .='<li><a href="'.esc_url($bizness_link8).'"><i class="icon-chevron-small-right"></i>'.esc_attr($bizness_text8).'</a></li>';
          endif;          
          if( !empty($bizness_text9) ):
          $output .='<li><a href="'.esc_url($bizness_link9).'"><i class="icon-chevron-small-right"></i>'.esc_attr($bizness_text9).'</a></li>';
          endif;          
          if( !empty($bizness_text10) ):
          $output .='<li><a href="'.esc_url($bizness_link10).'"><i class="icon-chevron-small-right"></i>'.esc_attr($bizness_text10).'</a></li>';
          endif;

        $output .='</ul>';


        echo wp_kses($output, $allowed_html_array);
  }

  function update( $new_instance, $old_instance ){

    $instance = $old_instance;
    $instance['title']                        = strip_tags( $new_instance['title'] );
    $instance['bizness_text1']                   = strip_tags( $new_instance['bizness_text1'] );
    $instance['bizness_link1']                   = strip_tags( $new_instance['bizness_link1'] );

    $instance['bizness_text2']                   = strip_tags( $new_instance['bizness_text2'] );
    $instance['bizness_link2']                   = strip_tags( $new_instance['bizness_link2'] );

    $instance['bizness_text3']                   = strip_tags( $new_instance['bizness_text3'] );
    $instance['bizness_link3']                   = strip_tags( $new_instance['bizness_link3'] );

    $instance['bizness_text4']                   = strip_tags( $new_instance['bizness_text4'] );
    $instance['bizness_link4']                   = strip_tags( $new_instance['bizness_link4'] );

    $instance['bizness_text5']                   = strip_tags( $new_instance['bizness_text5'] );
    $instance['bizness_link5']                   = strip_tags( $new_instance['bizness_link5'] );

    $instance['bizness_text6']                   = strip_tags( $new_instance['bizness_text6'] );
    $instance['bizness_link6']                   = strip_tags( $new_instance['bizness_link6'] );

    $instance['bizness_text7']                   = strip_tags( $new_instance['bizness_text7'] );
    $instance['bizness_link7']                   = strip_tags( $new_instance['bizness_link7'] );

    $instance['bizness_text8']                   = strip_tags( $new_instance['bizness_text8'] );
    $instance['bizness_link8']                   = strip_tags( $new_instance['bizness_link8'] );

    $instance['bizness_text9']                   = strip_tags( $new_instance['bizness_text9'] );
    $instance['bizness_link9']                   = strip_tags( $new_instance['bizness_link9'] );

    $instance['bizness_text10']                  = strip_tags( $new_instance['bizness_text10'] );
    $instance['bizness_link10']                  = strip_tags( $new_instance['bizness_link10'] );
    return $instance;

  }


  function form($instance){
    $defaults = array( 
      'title'                           => 'Quick Links',
      'bizness_text1'                      => '',
      'bizness_link1'                      => '',
      'bizness_text2'                      => '',
      'bizness_link2'                      => '',
      'bizness_text3'                      => '',
      'bizness_link3'                      => '',
      'bizness_text4'                      => '',
      'bizness_link4'                      => '',
      'bizness_text5'                      => '',
      'bizness_link5'                      => '',
      'bizness_text6'                      => '',
      'bizness_link6'                      => '',
      'bizness_text7'                      => '',
      'bizness_link7'                      => '',
      'bizness_text8'                      => '',
      'bizness_link8'                      => '',
      'bizness_text9'                      => '',
      'bizness_link9'                      => '',
      'bizness_text10'                     => '',
      'bizness_link10'                     => '',

    );
    $instance = wp_parse_args( (array) $instance, $defaults );
  ?>

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php esc_html_e('Heading', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo esc_attr($instance['title']); ?>" style="width:100%;" />
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_text1' )); ?>"><?php esc_html_e('First Text', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_text1' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_text1' )); ?>" value="<?php echo esc_attr($instance['bizness_text1']); ?>" style="width:100%;" />

      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_link1' )); ?>"><?php esc_html_e('First URL', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_link1' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_link1' )); ?>" value="<?php echo esc_attr($instance['bizness_link1']); ?>" style="width:100%;" />
    </p>       

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_text2' )); ?>"><?php esc_html_e('Second Text', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_text2' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_text2' )); ?>" value="<?php echo esc_attr($instance['bizness_text2']); ?>" style="width:100%;" />

      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_link2' )); ?>"><?php esc_html_e('Second URL', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_link2' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_link2' )); ?>" value="<?php echo esc_attr($instance['bizness_link2']); ?>" style="width:100%;" />
    </p>       

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_text3' )); ?>"><?php esc_html_e('Third Text', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_text3' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_text3' )); ?>" value="<?php echo esc_attr($instance['bizness_text3']); ?>" style="width:100%;" />

      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_link3' )); ?>"><?php esc_html_e('Third URL', 'bizness'); ?></label>
      <input type="text" id="<?php echo esc_attr($this->get_field_id( 'bizness_link3' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_link3' )); ?>" value="<?php echo esc_attr($instance['bizness_link3']); ?>" style="width:100%;"/>
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_text4' )); ?>"><?php esc_html_e('Fourth Text', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_text4' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_text4' )); ?>" value="<?php echo esc_attr($instance['bizness_text4']); ?>" style="width:100%;" />

      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_link4' )); ?>"><?php esc_html_e('Fourth URL', 'bizness'); ?></label>
      <input type="text" id="<?php echo esc_attr($this->get_field_id( 'bizness_link4' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_link4' )); ?>" value="<?php echo esc_attr($instance['bizness_link4']); ?>" style="width:100%;"/>
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_text5' )); ?>"><?php esc_html_e('Fifth Text', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_text5' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_text5' )); ?>" value="<?php echo esc_attr($instance['bizness_text5']); ?>" style="width:100%;" />

      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_link5' )); ?>"><?php esc_html_e('Fifth URL', 'bizness'); ?></label>
      <input type="text" id="<?php echo esc_attr($this->get_field_id( 'bizness_link5' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_link5' )); ?>" value="<?php echo esc_attr($instance['bizness_link5']); ?>" style="width:100%;"/>
    </p>    
    <hr>
    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_text6' )); ?>"><?php esc_html_e('Sixth Text', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_text6' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_text6' )); ?>" value="<?php echo esc_attr($instance['bizness_text6']); ?>" style="width:100%;" />

      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_link6' )); ?>"><?php esc_html_e('Sixth URL', 'bizness'); ?></label>
      <input type="text" id="<?php echo esc_attr($this->get_field_id( 'bizness_link6' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_link6' )); ?>" value="<?php echo esc_attr($instance['bizness_link6']); ?>" style="width:100%;" />
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_text7' )); ?>"><?php esc_html_e('Seventh Text', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_text7' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_text7' )); ?>" value="<?php echo esc_attr($instance['bizness_text7']); ?>" style="width:100%;" />

      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_link7' )); ?>"><?php esc_html_e('Seventh  URL', 'bizness'); ?></label>
      <input type="text" id="<?php echo esc_attr($this->get_field_id( 'bizness_link7' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_link7' )); ?>" value="<?php echo esc_attr($instance['bizness_link7']); ?>"style="width:100%;" />
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_text8' )); ?>"><?php esc_html_e('Eight Text', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_text8' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_text8' )); ?>" value="<?php echo esc_attr($instance['bizness_text8']); ?>" style="width:100%;" />

      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_link8' )); ?>"><?php esc_html_e('Eight URL', 'bizness'); ?></label>
      <input type="text" id="<?php echo esc_attr($this->get_field_id( 'bizness_link8' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_link8' )); ?>" value="<?php echo esc_attr($instance['bizness_link8']); ?>" style="width:100%;" />
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_text9' )); ?>"><?php esc_html_e('Nine Text', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_text9' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_text9' )); ?>" value="<?php echo esc_attr($instance['bizness_text9']); ?>" style="width:100%;" />

      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_link9' )); ?>"><?php esc_html_e('Nine URL', 'bizness'); ?></label>
      <input type="text" id="<?php echo esc_attr($this->get_field_id( 'bizness_link9' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_link9' )); ?>" value="<?php echo esc_attr($instance['bizness_link9']); ?>" style="width:100%;">
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_text10' )); ?>"><?php esc_html_e('Tenth Text', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_text10' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_text10' )); ?>" value="<?php echo esc_attr($instance['bizness_text10']); ?>" style="width:100%;" />

      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_link10' )); ?>"><?php esc_html_e('Tenth URL', 'bizness'); ?></label>
      <input type="text" id="<?php echo esc_attr($this->get_field_id( 'bizness_link10' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_link10' )); ?>" value="<?php echo esc_attr($instance['bizness_link10']); ?>" style="width:100%;">
    </p>

  <?php
  }

}//end of class

// register bizness_about_Widget widget
function bizness_register_quick_links_Widget() {
  register_widget( 'bizness_quick_links_Widget' );
}
add_action( 'widgets_init', 'bizness_register_quick_links_Widget' );


/*************************
Start Bizness Keep in Touch
*************************/
class bizness_keep_in_touch_Widget extends WP_Widget {

  function __construct() {
    parent::__construct(
      'bizness_keep_in_touch_Widget', // Base ID
      esc_html('bizness -Keep in Touch Widget', 'bizness'), // Name
      array( 'description' => esc_html( 'Display Keep in Touch. Works in Footer.', 'bizness' ), ) // Args
    );
  }


      /*-------------------------------------------------------
       *        Front-end display of widget
       *-------------------------------------------------------*/


  public function widget( $args, $instance ) {

        extract($args);

        $title                            = apply_filters('widget_title', $instance['title'] );
        $bizness_address                     = esc_attr($instance['bizness_address']);
        $bizness_contact                     = esc_attr($instance['bizness_contact']);
        $bizness_email                       = esc_attr($instance['bizness_email']);
        $bizness_mapimg_url                  = esc_attr($instance['bizness_mapimg_url']);

        $allowed_html_array = array(
        'h3' => array(
          'class' => array()
        ),
        'span' => array(
          'class' => array()
        ),
        'p' => array(
          'class' => array()
        ),
        'i' => array(
          'class' => array()
        ),
        'a' => array(
          'class' => array(),
          'href' => array()
        ),        
        'img' => array(
          'class' => array(),
          'src' => array(),
          'alt' => array()
        ));

        //echo ;
        $output = '';
        $output .= '<h3 class="heading bottom25">'.$title.'<span class="divider-left"></span></h3>';
        $output .='<p class=" address"><i class="icon-map-pin"></i>'.$bizness_address.'</p>';
        $output .='<p class=" address"><i class="icon-phone"></i>'.$bizness_contact.'</p>';
        $output .='<p class=" address"><i class="icon-envelope"></i><a href="mailto:'.$bizness_email.'">'.$bizness_email.'</a></p>';
        if( !empty($bizness_mapimg_url) ){
        $output .='<img src="'.$bizness_mapimg_url.'" alt="" class="img-responsive">';
        }

        echo wp_kses($output, $allowed_html_array);
  }

  function update( $new_instance, $old_instance ){

    $instance = $old_instance;
    $instance['title']                          = strip_tags( $new_instance['title'] );
    $instance['bizness_address']                   = strip_tags( $new_instance['bizness_address'] );
    $instance['bizness_contact']                   = strip_tags( $new_instance['bizness_contact'] );
    $instance['bizness_email']                     = strip_tags( $new_instance['bizness_email'] );
    $instance['bizness_mapimg_url']                = strip_tags( $new_instance['bizness_mapimg_url'] );

    return $instance;

  }


  function form($instance){
    $defaults = array( 
      'title'                             => 'Quick Links',
      'bizness_address'                      => '',
      'bizness_contact'                      => '',
      'bizness_email'                        => '',
      'bizness_mapimg_url'                   => '',

    );
    $instance = wp_parse_args( (array) $instance, $defaults );
  ?>

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php esc_html_e('Heading', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo esc_attr($instance['title']); ?>" style="width:100%;" />
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_address' )); ?>"><?php esc_html_e('Address', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_address' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_address' )); ?>" value="<?php echo esc_attr($instance['bizness_address']); ?>" style="width:100%;" />
    </p>       

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_contact' )); ?>"><?php esc_html_e('Contact No', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_contact' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_contact' )); ?>" value="<?php echo esc_attr($instance['bizness_contact']); ?>" style="width:100%;" />
    </p>       

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_email' )); ?>"><?php esc_html_e('Email', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_email' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_email' )); ?>" value="<?php echo esc_attr($instance['bizness_email']); ?>" style="width:100%;" />
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_mapimg_url' )); ?>"><?php esc_html_e('Map Image URL', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_mapimg_url' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_mapimg_url' )); ?>" value="<?php echo esc_attr($instance['bizness_mapimg_url']); ?>" style="width:100%;" />
    </p>    

  <?php
  }

}//end of class

// register bizness_keep_in_touch_Widget widget
function bizness_register_keep_in_touch_Widget() {
  register_widget( 'bizness_keep_in_touch_Widget' );
}
add_action( 'widgets_init', 'bizness_register_keep_in_touch_Widget' );


/*************************
Start Bizness Working Schedule
*************************/
class bizness_working_schedule_Widget extends WP_Widget {

  function __construct() {
    parent::__construct(
      'bizness_working_schedule_Widget', // Base ID
      esc_html('bizness -Working Schedule Widget', 'bizness'), // Name
      array( 'description' => esc_html( 'Display Working Schedule. Works in Footer.', 'bizness' ), ) // Args
    );
  }


      /*-------------------------------------------------------
       *        Front-end display of widget
       *-------------------------------------------------------*/


  public function widget( $args, $instance ) {

        extract($args);

        $title                                = apply_filters('widget_title', $instance['title'] );

        $bizness_mstart_time                     = esc_attr($instance['bizness_mstart_time']);
        $bizness_mend_time                       = esc_attr($instance['bizness_mend_time']);
        $bizness_mclose                          = $instance[ 'bizness_mclose' ] ? 'true' : 'false';              

        $bizness_tstart_time                     = esc_attr($instance['bizness_tstart_time']);
        $bizness_tend_time                       = esc_attr($instance['bizness_tend_time']);        
        $bizness_tclose                          = $instance[ 'bizness_tclose' ] ? 'true' : 'false';        

        $bizness_wstart_time                     = esc_attr($instance['bizness_wstart_time']);
        $bizness_wend_time                       = esc_attr($instance['bizness_wend_time']);        
        $bizness_wclose                          = $instance[ 'bizness_wclose' ] ? 'true' : 'false';              

        $bizness_thstart_time                    = esc_attr($instance['bizness_thstart_time']);
        $bizness_thend_time                      = esc_attr($instance['bizness_thend_time']);        
        $bizness_thclose                         = $instance[ 'bizness_thclose' ] ? 'true' : 'false';           

        $bizness_frstart_time                    = esc_attr($instance['bizness_frstart_time']);
        $bizness_frend_time                      = esc_attr($instance['bizness_frend_time']);        
        $bizness_frclose                         = esc_attr($instance['bizness_frclose']);
        $bizness_frclose                         = $instance[ 'bizness_frclose' ] ? 'true' : 'false';                  

        $bizness_ststart_time                    = esc_attr($instance['bizness_ststart_time']);
        $bizness_stend_time                      = esc_attr($instance['bizness_stend_time']);        
        $bizness_stclose                         = $instance[ 'bizness_stclose' ] ? 'true' : 'false';          

        $bizness_sunstart_time                   = esc_attr($instance['bizness_sunstart_time']);
        $bizness_sunend_time                     = esc_attr($instance['bizness_sunend_time']);
        $bizness_sunclose                        = $instance[ 'bizness_sunclose' ] ? 'true' : 'false';

        $allowed_html_array = array(
        'div' => array(
          'class' => array(),
          'id' => array(),
          'style' => array()
        ),          
        'h3' => array(
          'class' => array()
        ),
        'p' => array(
          'class' => array()
        ),        
        'span' => array(
          'class' => array()
        ),
        'a' => array(
          'class' => array(),
          'href' => array()
        ));        

        //echo ;
        $output = '';
        $output .='<div class="widget heading_space">';
          $output .='<h3 class="bottom20">'.$title.'</h3>';
          if( $instance[ 'bizness_mclose' ] != 'on' ){
          $output .='<p class="hours"> Monday <span>'.$bizness_mstart_time.' - '.$bizness_mend_time.'</span></p>';
          } else {
          $output .='<p class="hours"> Monday <span><a href="#." class="border_radius text-uppercase">closed</a></span></p>';
          }
          if( $instance[ 'bizness_tclose' ] != 'on' ){
          $output .='<p class="hours"> Tuesday <span>'.$bizness_tstart_time.' - '.$bizness_tend_time.'</span></p>';
          } else {
          $output .='<p class="hours"> Tuesday <span><a href="#." class="border_radius text-uppercase">closed</a></span></p>';
          }

          if( $instance[ 'bizness_wclose' ] != 'on' ){
          $output .='<p class="hours"> Wednesday <span>'.$bizness_wstart_time.' - '.$bizness_wend_time.'</span></p>';
          } else {
          $output .='<p class="hours"> Wednesday <span><a href="#." class="border_radius text-uppercase">closed</a></span></p>';     
          } 

          if( $instance[ 'bizness_thclose' ] != 'on' ){
          $output .='<p class="hours"> Thursday <span>'.$bizness_thstart_time.' - '.$bizness_thend_time.'</span></p>';
          } else {
          $output .='<p class="hours"> Thursday <span><a href="#." class="border_radius text-uppercase">closed</a></span></p>';   
          }

          if( $instance[ 'bizness_frclose' ] != 'on' ){
          $output .='<p class="hours"> Friday <span>'.$bizness_frstart_time.' - '.$bizness_frend_time.'</span></p>';
          } else {
          $output .='<p class="hours"> Friday <span><a href="#." class="border_radius text-uppercase">closed</a></span></p>';      
          }

          if( $instance[ 'bizness_stclose' ] != 'on' ){
          $output .='<p class="hours"> Saturday <span>'.$bizness_ststart_time.' - '.$bizness_stend_time.'</span></p>';
          } else {
          $output .='<p class="hours"> Saturday <span><a href="#." class="border_radius text-uppercase">closed</a></span></p>';      
          }

          if( $instance[ 'bizness_sunclose' ] != 'on' ){
          $output .='<p class="hours"> Sunday <span>'.$bizness_sunstart_time.' - '.$bizness_sunend_time.'</span></p>';
          } else {
          $output .='<p class="hours"> Sunday <span><a href="#." class="border_radius text-uppercase">closed</a></span></p>';
          }

        $output .='</div>';

        echo wp_kses($output, $allowed_html_array);
  }

  function update( $new_instance, $old_instance ){

    $instance = $old_instance;
    $instance['title']                              = strip_tags( $new_instance['title'] );
    $instance['bizness_monday']                        = strip_tags( $new_instance['bizness_monday'] );
    $instance['bizness_mstart_time']                   = strip_tags( $new_instance['bizness_mstart_time'] );
    $instance['bizness_mend_time']                     = strip_tags( $new_instance['bizness_mend_time'] );    
    $instance['bizness_mclose']                        = strip_tags( $new_instance['bizness_mclose'] );    

    $instance['bizness_tuesday']                       = strip_tags( $new_instance['bizness_tuesday'] );
    $instance['bizness_tstart_time']                   = strip_tags( $new_instance['bizness_tstart_time'] );
    $instance['bizness_tend_time']                     = strip_tags( $new_instance['bizness_tend_time'] );    
    $instance['bizness_tclose']                        = strip_tags( $new_instance['bizness_tclose'] );    

    $instance['bizness_wednesday']                     = strip_tags( $new_instance['bizness_wednesday'] );
    $instance['bizness_wstart_time']                   = strip_tags( $new_instance['bizness_wstart_time'] );
    $instance['bizness_wend_time']                     = strip_tags( $new_instance['bizness_wend_time'] );    
    $instance['bizness_wclose']                        = strip_tags( $new_instance['bizness_wclose'] );    

    $instance['bizness_thursday']                      = strip_tags( $new_instance['bizness_thursday'] );
    $instance['bizness_thstart_time']                  = strip_tags( $new_instance['bizness_thstart_time'] );
    $instance['bizness_thend_time']                    = strip_tags( $new_instance['bizness_thend_time'] );    
    $instance['bizness_thclose']                       = strip_tags( $new_instance['bizness_thclose'] );    

    $instance['bizness_friday']                        = strip_tags( $new_instance['bizness_friday'] );
    $instance['bizness_frstart_time']                  = strip_tags( $new_instance['bizness_frstart_time'] );
    $instance['bizness_frend_time']                    = strip_tags( $new_instance['bizness_frend_time'] );    
    $instance['bizness_frclose']                       = strip_tags( $new_instance['bizness_frclose'] );    

    $instance['bizness_saturday']                      = strip_tags( $new_instance['bizness_saturday'] );
    $instance['bizness_ststart_time']                  = strip_tags( $new_instance['bizness_ststart_time'] );
    $instance['bizness_stend_time']                    = strip_tags( $new_instance['bizness_stend_time'] );    
    $instance['bizness_stclose']                       = strip_tags( $new_instance['bizness_stclose'] );    

    $instance['bizness_sunday']                        = strip_tags( $new_instance['bizness_sunday'] );
    $instance['bizness_sunstart_time']                 = strip_tags( $new_instance['bizness_sunstart_time'] );
    $instance['bizness_sunend_time']                   = strip_tags( $new_instance['bizness_sunend_time'] );
    $instance['bizness_sunclose']                      = strip_tags( $new_instance['bizness_sunclose'] );

    return $instance;

  }


  function form($instance){
    $defaults = array( 
      'title'                                  => 'Working Schedule',
      'bizness_monday'                            => '',
      'bizness_mstart_time'                       => '',
      'bizness_mend_time'                         => '',
      'bizness_mclose'                            => '',

      'bizness_tuesonday'                         => '',
      'bizness_tstart_time'                       => '',
      'bizness_tend_time'                         => '',  
      'bizness_tclose'                            => '',

      'bizness_wednesday'                         => '',
      'bizness_wstart_time'                       => '',
      'bizness_wend_time'                         => '',
      'bizness_wclose'                            => '',   

      'bizness_thursday'                          => '',
      'bizness_thstart_time'                      => '',
      'bizness_thend_time'                        => '',  
      'bizness_thclose'                           => '',    
                      
      'bizness_friday'                            => '',
      'bizness_frstart_time'                      => '',
      'bizness_frend_time'                        => '',
      'bizness_frclose'                           => '',          

      'bizness_saturday'                          => '',
      'bizness_ststart_time'                      => '',
      'bizness_stend_time'                        => '',
      'bizness_stclose'                           => '',       

      'bizness_sunday'                            => '',
      'bizness_sunstart_time'                     => '',
      'bizness_sunend_time'                       => '', 
      'bizness_sunclose'                          => '',        
    );
    $instance = wp_parse_args( (array) $instance, $defaults );
  ?>

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php esc_html_e('Heading', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo esc_attr($instance['title']); ?>" style="width:100%;" />
    </p>    

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_monday' )); ?>"><strong><?php esc_html_e('Monday', 'bizness'); ?></strong></label>
    </p>       

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_mstart_time' )); ?>"><?php esc_html_e('Start Time', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_mstart_time' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_mstart_time' )); ?>" value="<?php echo esc_attr($instance['bizness_mstart_time']); ?>" type="time" style="width:100%;" />
    </p>       

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_mend_time' )); ?>"><?php esc_html_e('End Time', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_mend_time' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_mend_time' )); ?>" value="<?php echo esc_attr($instance['bizness_mend_time']); ?>" type="time" style="width:100%;" />
      <input type="checkbox" name="<?php echo esc_attr($this->get_field_name( 'bizness_mclose' )); ?>" id="<?php echo esc_attr($this->get_field_id( 'bizness_mclose' )); ?>" value="<?php echo esc_attr($instance['bizness_mclose']); ?>">Off Day
    </p>    
    <hr> 

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_tuesday' )); ?>"><strong><?php esc_html_e('Tuesday', 'bizness'); ?></strong></label>
    </p>       
    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_tstart_time' )); ?>"><?php esc_html_e('Start Time', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_tstart_time' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_tstart_time' )); ?>" value="<?php echo esc_attr($instance['bizness_tstart_time']); ?>" type="time" style="width:100%;" />
    </p>       

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_tend_time' )); ?>"><?php esc_html_e('End Time', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_tend_time' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_tend_time' )); ?>" value="<?php echo esc_attr($instance['bizness_tend_time']); ?>" type="time" style="width:100%;" />
      <input type="checkbox" name="<?php echo esc_attr($this->get_field_name( 'bizness_tclose' )); ?>" id="<?php echo esc_attr($this->get_field_id( 'bizness_tclose' )); ?>" value="<?php echo esc_attr($instance['bizness_tclose']); ?>">Off Day
    </p>    
    <hr>      

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_wednesday' )); ?>"><strong><?php esc_html_e('Wednesday', 'bizness'); ?></strong></label>
    </p>       
    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_wstart_time' )); ?>"><?php esc_html_e('Start Time', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_wstart_time' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_wstart_time' )); ?>" value="<?php echo esc_attr($instance['bizness_wstart_time']); ?>" type="time" style="width:100%;" />
    </p>       

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_wend_time' )); ?>"><?php esc_html_e('End Time', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_wend_time' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_wend_time' )); ?>" value="<?php echo esc_attr($instance['bizness_wend_time']); ?>" type="time" style="width:100%;" />
      <input type="checkbox" name="<?php echo esc_attr($this->get_field_name( 'bizness_wclose' )); ?>" id="<?php echo esc_attr($this->get_field_id( 'bizness_wclose' )); ?>"  <?php checked( $instance[ 'bizness_wclose' ], 'on' ); ?>>Off Day
    </p>    
    <hr>

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_thursday' )); ?>"><strong><?php esc_html_e('Thursday', 'bizness'); ?></strong></label>
    </p>       
    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_thstart_time' )); ?>"><?php esc_html_e('Start Time', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_thstart_time' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_thstart_time' )); ?>" value="<?php echo esc_attr($instance['bizness_thstart_time']); ?>" type="time" style="width:100%;" />
    </p>       

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_thend_time' )); ?>"><?php esc_html_e('End Time', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_thend_time' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_thend_time' )); ?>" value="<?php echo esc_attr($instance['bizness_thend_time']); ?>" type="time" style="width:100%;" />
      <input type="checkbox" name="<?php echo esc_attr($this->get_field_name( 'bizness_thclose' )); ?>" id="<?php echo esc_attr($this->get_field_id( 'bizness_thclose' )); ?>" <?php checked( $instance[ 'bizness_thclose' ], 'on' ); ?>>Off Day
    </p>    
    <hr> 

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_friday' )); ?>"><strong><?php esc_html_e('Friday', 'bizness'); ?></strong></label>
    </p>       
    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_frstart_time' )); ?>"><?php esc_html_e('Start Time', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_frstart_time' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_frstart_time' )); ?>" value="<?php echo esc_attr($instance['bizness_frstart_time']); ?>" type="time" style="width:100%;" />
    </p>       

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_frend_time' )); ?>"><?php esc_html_e('End Time', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_frend_time' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_frend_time' )); ?>" value="<?php echo esc_attr($instance['bizness_frend_time']); ?>" type="time" style="width:100%;" />
      <input type="checkbox" name="<?php echo esc_attr($this->get_field_name( 'bizness_frclose' )); ?>" <?php checked( $instance[ 'bizness_frclose' ], 'on' ); ?>>Off Day
    </p>    
    <hr> 

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_saturday' )); ?>"><strong><?php esc_html_e('Saturday', 'bizness'); ?></strong></label>
    </p>       
    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_ststart_time' )); ?>"><?php esc_html_e('Start Time', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_ststart_time' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_ststart_time' )); ?>" value="<?php echo esc_attr($instance['bizness_ststart_time']); ?>" type="time" style="width:100%;" />
    </p>       

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_stend_time' )); ?>"><?php esc_html_e('End Time', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_stend_time' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_stend_time' )); ?>" value="<?php echo esc_attr($instance['bizness_stend_time']); ?>" type="time" style="width:100%;" />
      <input type="checkbox" name="<?php echo esc_attr($this->get_field_name( 'bizness_stclose' )); ?>" id="<?php echo esc_attr($this->get_field_id( 'bizness_stclose' )); ?>" <?php checked( $instance[ 'bizness_stclose' ], 'on' ); ?>>Off Day
    </p>    
    <hr> 

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_sunday' )); ?>"><strong><?php esc_html_e('Sunday', 'bizness'); ?></strong></label>
    </p>       
    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_sunstart_time' )); ?>"><?php esc_html_e('Start Time', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_sunstart_time' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_sunstart_time' )); ?>" value="<?php echo esc_attr($instance['bizness_sunstart_time']); ?>" type="time" style="width:100%;" />
    </p>       

    <p>
      <label for="<?php echo esc_attr($this->get_field_id( 'bizness_sunend_time' )); ?>"><?php esc_html_e('End Time', 'bizness'); ?></label>
      <input id="<?php echo esc_attr($this->get_field_id( 'bizness_sunend_time' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bizness_sunend_time' )); ?>" value="<?php echo esc_attr($instance['bizness_sunend_time']); ?>" type="time" style="width:100%;" />
      <input type="checkbox" name="<?php echo esc_attr($this->get_field_name( 'bizness_sunclose' )); ?>" id="<?php echo esc_attr($this->get_field_id( 'bizness_sunclose' )); ?>"  <?php checked( $instance[ 'bizness_sunclose' ], 'on' ); ?>>Off Day
    </p>    
    <hr>                          

  <?php
  }

}//end of class

// register bizness_keep_in_touch_Widget widget
function bizness_register_working_schedule_Widget() {
  register_widget( 'bizness_working_schedule_Widget' );
}
add_action( 'widgets_init', 'bizness_register_working_schedule_Widget' );