<?php
/**
 * The template for displaying Comments.
 */

if ( post_password_required() )
	return;
?>


	<?php // You can start editing here -- including this comment! ?>
	<?php if ( have_comments() ) : ?>

		<h3 class="heading bottom25">
          <?php comments_number(esc_html('No Comment Yet', 'bizness'), esc_html('1 Comment', 'bizness'), esc_html('% Comments', 'bizness') );?>
          <span class="divider-left"></span>
        </h3>
      <?php wp_list_comments( array( 'callback' => 'bizness_comment', 'style' => 'div' ) ); ?>
	  <div class="comments_paginate">
		<?php  paginate_comments_links(); ?>
	  </div>	      

	<?php endif; // have_comments() ?>

	<?php if ( comments_open() ) : ?>
			<?php if ( is_user_logged_in() ) { ?>
				<?php comment_form(array(
				'class_form'      		=> 'findus heading_space',
				'title_reply' 			=> 'Leave a Comment',
				'title_reply_before' 	=> '<h2 class="heading bottom25">',
				'title_reply_after' 	=> '<span class="divider-left"></span></h2>',
				'cancel_reply_link' 	=> '<kbd>'.esc_html( 'Cancel Reply' , 'bizness' ).'</kbd>',
				)); ?>
			<?php } else { ?>
				<?php comment_form(array(
				'class_form'     		 => 'findus heading_space',
				'title_reply'			 => 'Leave a Comment',
				'title_reply_before' 	 => '<h2 class="heading bottom25">',
				'title_reply_after' 	 => '<span class="divider-left"></span></h2>',
				'cancel_reply_link'      => '<kbd>'.esc_html( 'Cancel Reply' , 'bizness' ).'</kbd>',
				)); ?>
			<?php } ?>
	<?php endif; ?>		