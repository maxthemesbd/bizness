<?php
/**
 * Template Name: Testimonial
 */
get_header();
$bizness_page_heading              = get_post_meta(get_the_ID(), 'bizness_page_heading', true);
$bizness_page_subheading           = get_post_meta(get_the_ID(), 'bizness_page_subheading', true);
$bizness_page_bg                   = get_post_meta(get_the_ID(), 'bizness_page_bg', true);
$show_hide_page_header              = get_post_meta(get_the_ID(), 'show_hide_page_header', true);
if( $show_hide_page_header == "show" ){
?>
<!--Page Header-->
<section class="page_header padding-top" <?php if( !empty($bizness_page_bg)){ ?>style="background: url('<?php echo esc_url( $bizness_page_bg ); ?>');" <?php } else if(bizness_get_option('bizness_page_header_img') != ''){ ?>style="background: url('<?php echo esc_url( bizness_get_option('bizness_page_header_img') ); ?>');" <?php } else { ?>style="background: url('<?php echo esc_url( get_template_directory_uri() ).'/images/'; ?>page-tittle.jpg');"<?php } ?>>
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">

        <?php if( !empty($bizness_page_heading) ): ?>
          <h1><?php echo ucwords(strtolower(esc_attr($bizness_page_heading))); ?></h1>
        <?php else: ?>
          <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
          <h1><?php ucwords(the_title()); ?></h1>
          <?php endwhile; endif; ?>
        <?php endif; ?>

        <?php if( !empty($bizness_page_subheading) ): ?>
        <p><?php echo esc_attr( $bizness_page_subheading ); ?></p>
        <?php else: ?>        
        <p><?php echo bloginfo('name'); ?></p>
        <?php endif; ?> 

        <div class="page_nav">
          <?php if (function_exists('bizness_wordpress_breadcrumbs')) bizness_wordpress_breadcrumbs(); ?>
        </div>        
      </div>
    </div>
  </div>
</section>
<?php } ?>


<!--Testimonial-->
<section id="testinomila_page" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 wow fadeInDown">
        <h2 class="heading heading_space"><span><?php esc_html_e('Client', 'bizness'); ?></span> <?php esc_html_e('Testimonials', 'bizness'); ?><span class="divider-left"></span></h2>
      </div>
    </div>
    <div id="js-grid-masonry" class="cbp">
    <?php        
      if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
      elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
      else { $paged = 1; }
      $args = array('post_type'=> 'bizness-testimonial','order'=> 'DESC', 'paged' => $paged, 'orderby' => 'post_date' );
      query_posts($args);    
    ?>
    <?php if (have_posts()) :  while (have_posts()) : the_post(); 
      $bizness_global_post       	= bizness_get_global_post();
      $postid                 	= $bizness_global_post->ID;
      $get_image              	= esc_url( wp_get_attachment_url( get_post_thumbnail_id($postid) ) );  
      $testimonial_designation  = get_post_meta($post->ID, 'testimonial_designation', true);
              
    ?>      	
      <div class="cbp-item">
        <div class="cbp-caption-defaultWrap">
          <div class="testimonial_wrap">
            <div class="testimonial_text text-center">
              <img src="<?php echo get_template_directory_uri().'/images/quote.png'; ?>" alt="" class="quote">
              <?php the_content(); ?>
            </div>
            <div class="testimonial_pic">
          	<?php if ( has_post_thumbnail() ): ?>            	
              <img src="<?php echo esc_url($get_image); ?>" alt="" width="59">
          	<?php endif; ?>              
              <span class="color"><?php the_title(); ?></span>
              <span class="post_img"><?php echo esc_attr($testimonial_designation); ?></span>
            </div>
          </div>
        </div>
      </div>
      <?php endwhile; endif; ?>
    </div>
  </div>
</section>
<!--Testimonial ends-->


<?php get_footer(); ?>