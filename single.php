<?php
/**
 * Blog Single Page
 */
get_header(); 
$bizness_page_heading              = get_post_meta(get_the_ID(), 'bizness_page_heading', true);
$bizness_page_subheading           = get_post_meta(get_the_ID(), 'bizness_page_subheading', true);
$bizness_page_bg                   = get_post_meta(get_the_ID(), 'bizness_page_bg', true);
?>

<!--Page Header-->
<section class="page_header padding-top" <?php if( !empty($bizness_page_bg)){ ?>style="background: url('<?php echo esc_url( $bizness_page_bg ); ?>');" <?php } else if(bizness_get_option('bizness_page_header_img') != ''){ ?>style="background: url('<?php echo esc_url( bizness_get_option('bizness_page_header_img') ); ?>');" <?php } else { ?>style="background: url('<?php echo esc_url( get_template_directory_uri() ).'/images/'; ?>page-tittle.jpg');"<?php } ?>>
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <?php if( !empty($bizness_page_heading) ): ?>
        <h1><?php echo ucwords(strtolower(esc_attr($bizness_page_heading))); ?></h1>
        <?php else: ?>
        <h1><?php printf( esc_html( 'Blog Single', 'bizness' )); ?></h1>
        <?php endif; ?>        
        <p><?php bloginfo('description'); ?></p>
        <div class="page_nav">
          <?php if (function_exists('bizness_wordpress_breadcrumbs')) bizness_wordpress_breadcrumbs(); ?>
        </div>
      </div>
    </div>
  </div>
</section>



<!--BLOG SECTION-->
<section id="blog" class="padding-bottom-half padding-top">
 <h3 class="hidden"><?php esc_html_e('hidden', 'bizness'); ?></h3>
 <div class="container">
     <div class="row">
      <div class="col-md-9 col-sm-8 wow fadeIn" data-wow-delay="400ms">
      <?php if (have_posts()) :  while (have_posts()) : the_post(); 
          $bizness_global_post = bizness_get_global_post();
          $postid = $bizness_global_post->ID;
          $get_image = esc_url( wp_get_attachment_url( get_post_thumbnail_id($postid) ) );
      ?>        
        <article class="blog_item padding-bottom-half heading_space">
          <?php if( !empty($get_image) ): ?>
          <div class="image bottom25">
            <img src="<?php echo esc_url( $get_image ); ?>" alt="">
          </div>
          <?php endif; ?>
          <h3><?php the_title(); ?></h3>
          <ul class="comment margin10">
            <li><a href="<?php the_permalink();?>"><?php the_time( get_option('date_format') ); ?></a></li>
            <li><a href="<?php the_permalink();?>"><i class="icon-comment"></i> <?php echo get_comments_number( $postid ); ?></a></li>
          </ul>

          <?php the_content(); ?>
          <?php if ( has_tag() ) { ?>
          <div class="post_tags">
            <p class="tag_cate half_space"><strong><?php esc_html_e('Tags: ', 'bizness'); ?></strong> 
              <?php 
                $before = '';
                $seperator = ', '; // blank instead of comma
                $after = '';
                the_tags( $before, $seperator, $after );
              ?>              
            </p>
          </div>
          <?php } ?>

          <?php wp_link_pages( array(
            'before'      => '<ul class="blog-pagenation">',
            'after'       => '</ul>',
            'link_before' => '',
            'link_after'  => '',
            'next_or_number' => 'next',
            ) );
          ?> 
        </article>
        
        <div class="share clearfix heading_space">
          <p class="pull-left"><strong><?php esc_html_e('Share This Article:', 'bizness'); ?></strong></p>
          <ul class="pull-right">
            <li><a href="#" data-type="facebook" class="prettySocial"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#" data-type="twitter" class="prettySocial"><i class="icon-twitter4"></i></a></li>
          </ul>        
        </div>
        <div class="row">
          <div class="col-md-6">
          <?php $prevPost = get_previous_post(true);
          if($prevPost) {
          ?>
          <article class="blog_newest text-left heading_space border_radius">
            <h2 class="hidden"><?php esc_html_e('Share This Article:', 'bizness'); ?></h2>
              <span class="post_img">
                <?php 
                  if( !empty(get_the_post_thumbnail($prevPost->ID, array(69,65))) ){
                      echo get_the_post_thumbnail($prevPost->ID, array(69,65) );
                  } else { ?>
                      <img src="<?php echo get_template_directory_uri().'/images/post-thumb.png'; ?>" alt="">
                <?php } ?>
              </span>
              <div class="text">
              <i class="link"><?php esc_html_e('Previous Post', 'bizness'); ?></i>
              <?php previous_post_link('%link'); ?> 
            </div>
          </article>
          <?php } ?>
          </div>
          <div class="col-md-6">
           <?php $nextPost = get_next_post(true); 
           if($nextPost) {
           ?> 
          <article class="blog_newest text-right heading_space border_radius next-artical">
            <h2 class="hidden"><?php esc_html_e('Share This Article:', 'bizness'); ?></h2>
            <div class="text">
              <i class="link"><?php esc_html_e('Next Post', 'bizness'); ?></i>
              <?php next_post_link('%link'); ?>
            </div>
            <span class="post_img">
              <?php 
                if( !empty(get_the_post_thumbnail($nextPost->ID, array(69,65))) ){
                    echo get_the_post_thumbnail( $nextPost->ID, array(69,65) );
                  } else { ?>
                      <img src="<?php echo get_template_directory_uri().'/images/post-thumb.png'; ?>" alt="">
                <?php } ?>            
            </span>
          </article>
          <?php } ?>
          </div>
        </div>  
        <?php endwhile; endif; ?> 
        <article>
          <?php comments_template( '', true ); ?> 
        </article>
      </div>
      
      <!--Sidebar-->
      <?php get_sidebar(); ?>
    <!--Sidebar end-->    
    </div>
  </div>
</section>




<?php get_footer(); ?>