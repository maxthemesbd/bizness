<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

  <!-- Favicon -->
  <?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
    if( !empty(bizness_get_option('bizness_favicon'))){ ?>
    <link rel="shortcut icon" href="<?php echo esc_url( bizness_get_option('bizness_favicon') ); ?>">
  <?php } else {?>
  <?php } } ?>
  
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>

<!--Loader-->
<div class="loader">
  <div class="spinner centered">
  <div class="spinner-container container1">
    <div class="circle1"></div>
    <div class="circle2"></div>
    <div class="circle3"></div>
    <div class="circle4"></div>
  </div>
  <div class="spinner-container container2">
    <div class="circle1"></div>
    <div class="circle2"></div>
    <div class="circle3"></div>
    <div class="circle4"></div>
  </div>
</div>
</div>

<?php $header_style = get_post_meta( get_the_ID(), 'header_solid', true); ?>

<?php 
if ($header_style == 'solid_header') { ?>
  <?php if (bizness_get_option('topbar-en')): ?>
    <div class="topbar">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="pull-left">
            <?php if(bizness_get_option('bizness_slogan') != '') { ?>
            <span class="info"><a href="<?php echo esc_url( $bizness_slogan_url ); ?>"><?php echo esc_attr( bizness_get_option('bizness_slogan') ); ?></a></span>
            <?php } ?>
            <?php if(bizness_get_option('bizness_icon_text_one') != '') { ?>
            <span class="info"><i class="<?php echo esc_attr( bizness_get_option('bizness_icon_one') ); ?>"></i><?php echo esc_attr( bizness_get_option('bizness_icon_text_one') ); ?></span>
            <?php } ?>
            <?php if(bizness_get_option('bizness_icon_text_two') != '') { ?>
            <span class="info"><i class="<?php echo esc_attr( bizness_get_option('bizness_icon_two') ); ?>"></i><?php echo esc_attr( bizness_get_option('bizness_icon_text_two') ); ?></span>
            <?php } ?>
            </div>
            <ul class="social_top pull-right">
            <?php if(bizness_get_option('social_facebook') != '') { ?>
              <li><a href="<?php echo esc_url( bizness_get_option('social_facebook') ); ?>"><i class="fa fa-facebook"></i></a></li>
            <?php } ?>
            <?php if(bizness_get_option('social_twitter') != '') { ?>
              <li><a href="<?php echo esc_url( bizness_get_option('social_twitter') ); ?>"><i class="icon-twitter4"></i></a></li>
            <?php } ?>
            <?php if(bizness_get_option('social_googleplus') != '') { ?>
              <li><a href="<?php echo esc_url( bizness_get_option('social_googleplus') ); ?>"><i class="icon-google"></i></a></li>
            <?php } ?>        
            <?php if(bizness_get_option('social_instagram_url') != '') { ?>
              <li><a href="<?php echo esc_url( bizness_get_option('social_instagram_url') ); ?>"><i class="icon-instagram"></i></a></li>
            <?php } ?>
            <?php if(bizness_get_option('social_youtube') != '') { ?>
              <li><a href="<?php echo esc_url( bizness_get_option('social_youtube') ); ?>"><i class="icon-youtube"></i></a></li>
            <?php } ?>        
            <?php if(bizness_get_option('social_linkedin') != '') { ?>
              <li><a href="<?php echo esc_url( bizness_get_option('social_linkedin') ); ?>"><i class="icon-linkedin"></i></a></li>
            <?php } ?>        
            <?php if(bizness_get_option('social_flickr') != '') { ?>
              <li><a href="<?php echo esc_url( bizness_get_option('social_flickr') ); ?>"><i class="icon-flickr"></i></a></li>
            <?php } ?>        
            </ul>
          </div>
        </div>
      </div>
    </div>
  <?php endif ?>
<?php } ?>
<!--Header-->

<header>
    <?php
    if ( $header_style == 'solid_header' ) { ?>
      <nav class="navbar navbar-default navbar-sticky bootsnav on no-full">     
      <?php }elseif ($header_style == 'transparent_header') { ?>
      <nav class="navbar navbar-default navbar-fixed white bootsnav pushy on no-full no-background">
      <?php }else{ ?>
      <nav class="navbar navbar-default navbar-sticky bootsnav on no-full"> 
      <?php } ?>

      <div class="container"> 
          <div id="menu_bars" class="right">
              <span class="t1"></span>
              <span class="t2"></span>
              <span class="t3"></span>
          </div>

          <div class="search_btn btn_common"><i class="icon-icons185"></i></div>

          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
              <i class="fa fa-bars"></i>
            </button>
            
            <?php
              if( bizness_get_option('logo-text-en') ) {
                  echo bizness_get_option('logo-text'); 
              }else{
                  if( bizness_get_option('logo') != '' || bizness_get_option('scroll-logo') != '' ){ ?>
                    <?php 
                    if ( $header_style == 'solid_header' ){ ?>
                        <!-- Scroll Logo -->
                        <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                          <img class="logo logo-scrolled" alt="" src="<?php echo esc_url( bizness_get_option('scroll-logo') ); ?>">
                        </a>  
                    <?php } elseif ( $header_style == 'transparent_header' ) { ?>
                        <!-- Display Logo -->
                        <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                          <img class="logo logo-display" alt="" src="<?php echo esc_url( bizness_get_option('logo') ); ?>">
                        </a> 
                        <!-- Scroll Logo -->
                        <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                          <img class="logo logo-scrolled" alt="" src="<?php echo esc_url( bizness_get_option('scroll-logo') ); ?>">
                        </a>
                   <?php }else{ ?>
                        <!-- Display Logo -->
                        <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo logo-display" alt="" src="<?php echo esc_url( bizness_get_option('logo') ); ?>"></a> 
                        <!-- Scroll Logo -->
                        <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo logo-scrolled" alt="" src="<?php echo esc_url( bizness_get_option('scroll-logo') ); ?>"></a>
                   <?php } ?>   
                 <?php } else { ?>
                  <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo logo-display"  alt="" src="<?php echo esc_url( get_template_directory_uri() ).'/images/'; ?>logo.png"></a>
              <?php } } ?>
          </div>

          <div class="collapse navbar-collapse" id="navbar-menu">
            <?php
            if (has_nav_menu('bizness_primary_menu')) {
                    wp_nav_menu( array(
                        'theme_location'    => 'bizness_primary_menu',
                        'container'         => false,
                        'container_id'      => '',
                        'conatiner_class'   => '',
                        'menu_class'        => 'nav navbar-nav navbar-right', 
                        'echo'              => true,
                        'items_wrap'        => '<ul id="%1$s" class="%2$s" data-in="fadeInDown" data-out="fadeOut">%3$s</ul>',
                        'depth'             => 5, 
                        'walker'            => new bizness_walker_nav_menu
                    ) );
              }
            ?>        
          </div>
      </div> 
      <div class="sidebar_menu">
        <nav class="pushmenu pushmenu-right">
          <!--Logo-->
          <?php if(bizness_get_option('logo') != '') { ?>
            <a class="push-logo" href="index.html"><img src="<?php echo esc_url( bizness_get_option('scroll-logo') ); ?>" alt=""></a>
          <?php } ?>
          <!--Logo end-->
          <?php     
            if ( has_nav_menu( 'secondary_nav' ) ) {
                wp_nav_menu( array(
                    'theme_location'      => 'secondary_nav',
                    'container'           => false,
                    'menu_class'          => 'push_nav centered',
                    'fallback_cb'         => 'wp_page_menu',
                    'depth'               => 4,
                    'walker'              => new Bizness_Walker_Secondary_Nav_Menu()
                    )
                ); 
            }                             
          ?> 
          <div class="clearfix"></div>
          <ul class="social_icon black top25 bottom20">
            <?php if ( esc_url( bizness_get_option('social_facebook')) ): ?>
                <li><a href="<?php echo esc_url( bizness_get_option('social_facebook') ); ?>" class="facebook"><i class="fa fa-facebook"></i></a></li> 
            <?php endif ?>

            <?php if ( esc_url( bizness_get_option('social_twitter')) ): ?>
                <li><a href="<?php echo esc_url( bizness_get_option('social_twitter') ); ?>" class="twitter"><i class="icon-twitter4"></i></a></li>
            <?php endif ?>

            <?php if( esc_url(bizness_get_option('social_googleplus')) ): ?>
              <li><a href="<?php echo esc_url( bizness_get_option('social_googleplus') ); ?>" class="google"><i class="icon-google"></i></a></li>
            <?php endif ?>

            <?php if ( esc_url( bizness_get_option('social_instagram_url')) ): ?>
                <li><a href="<?php echo esc_url( bizness_get_option('social_instagram_url') ); ?>" class="instagram"><i class="icon-instagram"></i></a></li>
            <?php endif ?>

            <?php if( esc_url(bizness_get_option('social_linkedin')) ): ?>
              <li><a href="<?php echo esc_url( bizness_get_option('social_linkedin') ); ?>" class="linkedin"><i class="icon-linkedin"></i></a></li>
            <?php endif; ?>  
          </ul>
        </nav>
      </div> 
      </nav>
</header>

<?php echo get_search_form();?>
