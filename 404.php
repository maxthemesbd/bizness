<?php
/**
 * 404 Page
 */
get_header(); 
?>

<!--Page Header-->
<section class="page_header padding-top" <?php if(bizness_get_option('bizness_page_header_img') != ''){ ?>style="background: url('<?php echo esc_url( bizness_get_option('bizness_page_header_img') ); ?>');" <?php } else { ?>style="background: url('<?php echo esc_url( get_template_directory_uri() ).'/images/'; ?>page-tittle.jpg');"<?php } ?>>
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1><?php esc_html_e('404 Error', 'bizness'); ?></h1>
        <p><?php echo get_bloginfo('name'); ?></p>
        <div class="page_nav">
        <?php if (function_exists('bizness_wordpress_breadcrumbs')) bizness_wordpress_breadcrumbs(); ?>
      </div>
      </div>
    </div>
  </div>
</section>

<!--Error 404 SECTION-->
<section id="error" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
      <div class="error wow bounceIn" data-wow-delay="300ms">
         <h1><?php esc_html_e('404', 'bizness'); ?></h1>
         <h2><?php esc_html_e('404 Error', 'bizness'); ?></h2>
      </div>
      <p class="heading_space"><?php esc_html_e('We are sorry, the page you want isn\'t here anymore. May be one of the links below can help !', 'bizness'); ?></p>
      <a href="<?php echo esc_url(home_url('/')); ?>" class="btn_common blue border_radius wow fadeIn" data-wow-delay="400ms"><?php esc_html_e('Back to home', 'bizness'); ?></a> 
      <a href="<?php echo esc_url(home_url('/')); ?>" class="btn_common yellow border_radius wow fadeIn" data-wow-delay="400ms"><?php esc_html_e('Get a quote', 'bizness'); ?></a>
      </div>
    </div>
  </div>
</section>


<?php get_footer(); ?>