<?php
/**
* The template for displaying product content in the single-product.php template
*
* Override this template by copying it to yourtheme/woocommerce/content-single-product.php
*
* @author 		WooThemes
* @package 		WooCommerce/Templates
* @version     	5.0.0
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; # Exit if accessed directly
}

?>

<?php
	do_action( 'woocommerce_before_single_product' );
	if ( post_password_required() ) {
		echo get_the_password_form();
		return;
	}
?>

<div id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="row">
		<div class="col-md-5 col-sm-5 wow fadeInLeft animated">
			<div class="relative">
				<?php do_action( 'woocommerce_before_single_product_summary' ); ?>
			</div>
		</div>
		<div class="col-md-7 col-sm-7 shop_info wow fadeInRight animated">
			<h2 class="heading bottom25"><span><?php  esc_html_e('Product', 'bizness'); ?></span> <?php esc_html_e('Details', 'bizness'); ?><span class="divider-left"></span></h2>
			<?php do_action( 'woocommerce_single_product_summary' ); ?>

			<div class="share clearfix">
	          <p class="pull-left"><strong><?php  esc_html_e('Share Product:', 'bizness'); ?></strong></p>
	          <ul class="pull-right">
	            <li><a href="#" data-type="facebook" title="facebook" class="prettySocial fa fa-facebook"></a></li>
	            <li><a href="#" data-type="twitter" title="twitter" class="prettySocial fa fa-twitter"></a></li>
	            <li><a href="#" data-type="googleplus" title="googleplus" class="prettySocial fa fa-google-plus"></a></li>
	            <li><a href="#" data-type="pinterest" title="pinterest" class="prettySocial fa fa-pinterest"></a></li>
	            <li><a href="#" data-type="linkedin" title="linkedin" class="prettySocial fa fa-linkedin"></a></li>
	          </ul>
	        </div>
		</div>
	</div>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>

<div class="wow fadeInUp animated" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">
  	<?php do_action( 'woocommerce_after_single_product_summary' ); ?>
</div>

