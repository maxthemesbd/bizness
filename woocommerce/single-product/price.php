<?php
/**
* Single Product Price
* @see     https://docs.woocommerce.com/document/template-structure/
* @author  WooThemes
* @package WooCommerce/Templates
* @version 3.0.0
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; # Exit if accessed directly
}

global $product; ?>

<h4 class="price_product"><?php echo $product->get_price_html(); ?></h4>
