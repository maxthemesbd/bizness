<?php
/**
 * Related Products
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     5.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $woocommerce_loop;
$woocommerce_loop['columns'] = 3;

if ( $related_products ) : ?>
<section class="related products">
	<div class="row">
		<div class="col-md-12 wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
			<h2 class="heading">
				<span><?php esc_html_e( 'Related', 'bizness' ); ?></span>
				<?php esc_html_e( 'Products', 'bizness' ); ?>
				<span class="divider-left"></span>
			</h2>
		</div>

		<?php woocommerce_product_loop_start(); $count=1; ?>
			<?php foreach ( $related_products as $related_product ) : ?>
				<?php
					if( $count<=3 ){
					 	$post_object = get_post( $related_product->get_id() );
						setup_postdata( $GLOBALS['post'] =& $post_object );

						wc_get_template_part( 'content', 'product' );  
						$count++; 
					}
				?>
			<?php endforeach; ?>
		<?php woocommerce_product_loop_end(); ?>
	</div>
</section>
<?php endif;

wp_reset_postdata();