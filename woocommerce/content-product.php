<?php
/**
* The template for displaying product content within loops.
*
* Override this template by copying it to yourtheme/woocommerce/content-product.php
*
* 	@author 	WooThemes
* 	@package 	WooCommerce/Templates
* 	@version  	5.0.0
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; # Exit if accessed directly
}

global $product;
# Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

?>

<li class="max-shop-content-wrap col-sm-6 margin10 bottom15 col-md-<?php echo bizness_get_option('shop_column'); ?>">
	<div class="shopping_box">
		<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
		<div class="image">
			<?php do_action( 'woocommerce_before_shop_loop_item_title' ); ?>
			<div class="overlay border_radius">
				<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
			</div>
		</div>
		<div class="shop_content text-center">
			<a href="<?php the_permalink(); ?>" class="title_link"><?php do_action( 'woocommerce_shop_loop_item_title' ); ?></a>
			<?php 
				$args 	= array( 'taxonomy' => 'product_cat' );
				$terms 	= get_terms('product_cat', $args);
			    $count = count($terms); 
			    if ($count > 0) {
			        foreach ($terms as $term) {
			            echo '<P>'.$term->description.'</P>';
			        }
			    }
			?>
			<p><?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?></p>
		</div>
	</div>
</li>
