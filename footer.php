<?php if(bizness_get_option('bizness_footer_upper') == '1') {
  if ( is_active_sidebar( 'footer_1c_sidebar' )  || is_active_sidebar( 'footer_2c_sidebar' ) || is_active_sidebar( 'footer_3c_sidebar' )) :
 ?>
<!--Footer-->
<footer class="padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-4 footer_panel bottom25">
        <?php dynamic_sidebar('footer_1c_sidebar');?>
      </div>
      <div class="col-md-4 col-sm-4 footer_panel bottom25">
        <?php dynamic_sidebar('footer_2c_sidebar');?>
      </div>
      <div class="col-md-4 col-sm-4 footer_panel bottom25">
        <?php dynamic_sidebar('footer_3c_sidebar');?>
      </div>
    </div>
  </div>
</footer>

<?php endif; } if(bizness_get_option('bizness_footer_bottom') == '1') { ?>

<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <p>
        <?php if(bizness_get_option('copy_text') != '') { ?><?php echo esc_textarea( bizness_get_option('copy_text') ); ?>
        <?php } else { ?><?php esc_html_e('Copyright &copy; 2018 Bizness. All rights reserved.', 'bizness'); ?><?php } ?>
        </p>
      </div>
    </div>
  </div>
</div>
<!--FOOTER ends-->

<?php } ?>
<div id="style-selector">
  <div class="style-selector-wrapper" id="style-selector-wrapper">
     <span class="title">Choose Theme Options</span>
     <div>
        <span class="title-sub2">Predefined Color Skins</span>
        <ul class="styles">
           <li><a href="#" onClick="setActiveStyleSheet('anora'); return false;" title="Anora"><span class="pre-color-skin1"></span></a></li>
           <li><a href="#" onClick="setActiveStyleSheet('cumen'); return false;" title="Cumen"><span class="pre-color-skin2"></span></a></li>
           <li><a href="#" onClick="setActiveStyleSheet('fcgroup'); return false;" title="Fcgroup"><span class="pre-color-skin3"></span></a></li>
           <li><a href="#" onClick="setActiveStyleSheet('arient'); return false;" title="Arient"><span class="pre-color-skin4"></span></a></li>
           <!-- <li><a href="#" onClick="setActiveStyleSheet('lightgreen'); return false;" title="Light Green"><span class="pre-color-skin5"></span></a></li> -->
        </ul>
     </div>
    <a href="#" class="close icon-chevron-right" id="close-icon"><i class="fa fa-wrench"></i></a>
  </div>
</div>



  <?php
    /* Always have wp_footer() just before the closing </body>
    * tag of your theme, or you will break many plugins, which
    * generally use this hook to reference JavaScript files.
    */

    wp_footer();  
  ?>
</body>
</html>
