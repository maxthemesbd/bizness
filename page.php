<?php
/**
 * The default page template of this theme
 */
get_header();
$bizness_page_heading              = get_post_meta(get_the_ID(), 'bizness_page_heading', true);
$bizness_page_subheading           = get_post_meta(get_the_ID(), 'bizness_page_subheading', true);
$bizness_page_bg                   = get_post_meta(get_the_ID(), 'bizness_page_bg', true);
?>
<!--Page Header-->
<section class="page_header padding-top" <?php if(bizness_get_option('bizness_page_header_img') != ''){ ?>style="background: url('<?php echo esc_url( bizness_get_option('bizness_page_header_img') ); ?>');" <?php } else { ?>style="background: url('<?php echo esc_url( get_template_directory_uri() ).'/images/'; ?>page-tittle.jpg');"<?php } ?>>
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1>
  			<?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
  			<?php ucwords(the_title()); ?>
  			<?php endwhile; endif; ?>			
    	  </h1>
        <p><?php bloginfo('description'); ?></p>
        <div class="page_nav">
          <?php if (function_exists('bizness_wordpress_breadcrumbs')) bizness_wordpress_breadcrumbs(); ?>
        </div>        
      </div>
    </div>
  </div>
</section>


<!--BLOG SECTION-->
<section id="blog" class="padding">
  <div class="container">
    <h2 class="hidden"><?php esc_html_e('Page', 'bizness'); ?></h2>
    <div class="row">
      <div class="col-md-12">
  		<?php
  			if (have_posts()) :  while (have_posts()) : the_post(); 
  				the_content(); 
  			endwhile; endif;
  		?>
        <div class="clearfix heading_space"></div>       
        <article>
          <?php comments_template( '', true ); ?> 
        </article>    	
      </div>     
    </div>
   </div> 
</section>     	
<!--BLOG SECTION-->

<?php get_footer(); ?>