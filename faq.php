<?php
/**
 * Template Name: FAQ
 */
get_header();
$bizness_page_heading              = get_post_meta(get_the_ID(), 'bizness_page_heading', true);
$bizness_page_subheading           = get_post_meta(get_the_ID(), 'bizness_page_subheading', true);
$bizness_page_bg                   = get_post_meta(get_the_ID(), 'bizness_page_bg', true);
$show_hide_page_header              = get_post_meta(get_the_ID(), 'show_hide_page_header', true);
if( $show_hide_page_header == "show" ){
?>
<!--Page Header-->
<section class="page_header padding-top" <?php if( !empty($bizness_page_bg)){ ?>style="background: url('<?php echo esc_url( $bizness_page_bg ); ?>');" <?php } else if(bizness_get_option('bizness_page_header_img') != ''){ ?>style="background: url('<?php echo esc_url( bizness_get_option('bizness_page_header_img') ); ?>');" <?php } else { ?>style="background: url('<?php echo esc_url( get_template_directory_uri() ).'/images/'; ?>page-tittle.jpg');"<?php } ?>>
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">

        <?php if( !empty($bizness_page_heading) ): ?>
          <h1><?php echo ucwords(strtolower(esc_attr($bizness_page_heading))); ?></h1>
        <?php else: ?>
          <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
          <h1><?php ucwords(the_title()); ?></h1>
          <?php endwhile; endif; ?>
        <?php endif; ?>

        <?php if( !empty($bizness_page_subheading) ): ?>
        <p><?php echo esc_attr( $bizness_page_subheading ); ?></p>
        <?php else: ?>        
        <p><?php echo bloginfo('name'); ?></p>
        <?php endif; ?> 

        <div class="page_nav">
          <?php if (function_exists('bizness_wordpress_breadcrumbs')) bizness_wordpress_breadcrumbs(); ?>
        </div>        
      </div>
    </div>
  </div>
</section>
<?php } ?>


<!--SERVICE SECTION-->
<section id="faq" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="heading heading_space wow fadeInDown"><span><?php esc_html_e('Frequently', 'bizness'); ?></span> <?php esc_html_e('Asked', 'bizness'); ?><span class="divider-left"></span></h2>   
          <div class="faq_content wow fadeIn" data-wow-delay="400ms">
              <ul class="items">
          			<?php
                      $data_delay = 3; 
          			      if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
          			      elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
          			      else { $paged = 1; }
          			      $args = array('post_type'=> 'bizness-faq','order'=> 'DESC', 'paged' => $paged, 'orderby' => 'post_date' );
          			      query_posts($args);    
          			    ?>
          			    <?php if (have_posts()) :  while (have_posts()) : the_post(); 
          			?>              	
                <li><a href="#." ><?php the_title(); ?></a>
                  <ul class="sub-items">
                    <li>
                      <?php the_content(); ?>
                    </li>
                  </ul>
                </li>
			         <?php endwhile; endif; ?>                      
              </ul>
        </div>
      </div>
    </div>
  </div>
</section>


<div class="clearfix"></div>

<?php get_footer(); ?>