<!--Right Sidebar-->
<?php if ( is_active_sidebar( 'bizness_sidebar' ) ) { 
		if( is_singular('bizness-service') ) {
	?>
	<aside class="col-sm-4 wow fadeIn" data-wow-delay="400ms">
	<?php } else { ?>		
    <div class="col-md-3">
        <aside class="sidebar bg_grey border-radius wow fadeIn" data-wow-delay="300ms">
    <?php } ?>    	
	        <!--widget area-->
	        <?php dynamic_sidebar( 'bizness_sidebar' ); ?>
	<?php
		if( is_singular('bizness-service') ) {
	?>        
	        <!--End widget area-->    	
        </aside>
    <?php } else { ?>    				        
        	<!--End widget area-->    	
        </aside>
    </div> 
    <?php } ?>
<?php } ?>
<!--End Right Sidebar-->  