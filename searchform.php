<!--Search-->
<div id="xsearch">
  <button type="button" class="close">x</button>
  <form method="get" role="search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
  	<input id="s" type="search" placeholder="<?php esc_html_e('Search here....', 'bizness'); ?>" name="s">
    <button type="submit" class="btn btn_common blue" id="searchbtn" name="Go" value="Submit"><?php esc_html_e('Search', 'bizness'); ?></button>
  </form>
</div>