<?php
/**
 * Single post page of Services
 */
get_header(); 
$bizness_page_heading              = get_post_meta(get_the_ID(), 'bizness_page_heading', true);
$bizness_page_subheading           = get_post_meta(get_the_ID(), 'bizness_page_subheading', true);
$bizness_page_bg                   = get_post_meta(get_the_ID(), 'bizness_page_bg', true);
?>

<!--Page Header-->
<section class="page_header padding-top" <?php if( !empty($bizness_page_bg)){ ?>style="background: url('<?php echo esc_url( $bizness_page_bg ); ?>');" <?php } else if(bizness_get_option('bizness_page_header_img') != ''){ ?>style="background: url('<?php echo esc_url( bizness_get_option('bizness_page_header_img') ); ?>');" <?php } else { ?>style="background: url('<?php echo esc_url( get_template_directory_uri() ).'/images/'; ?>page-tittle.jpg');"<?php } ?>>
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <?php if( !empty($bizness_page_heading) ): ?>
        <h1><?php echo ucwords(strtolower(esc_attr($bizness_page_heading))); ?></h1>
        <?php else: ?>
        <h1><?php echo get_bloginfo('name'); ?></h1>
        <?php endif; ?>

        <?php if( !empty($bizness_page_subheading) ): ?>
        <p><?php echo esc_attr($bizness_page_subheading); ?></p>
         <?php else: ?>
        <p><?php bloginfo('description'); ?></p>
        <?php endif; ?>
        
        <div class="page_nav">
        <?php if (function_exists('bizness_wordpress_breadcrumbs')) bizness_wordpress_breadcrumbs(); ?>
      </div>
      </div>
    </div>
  </div>
</section>
<!--Page Header-->


<!-- Services -->
<section id="course_all" class="padding-bottom-half padding-top">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 course_detail wow fadeIn" data-wow-delay="400ms">
      <?php if (have_posts()) :  while (have_posts()) : the_post(); 
        $bizness_global_post           = bizness_get_global_post();
        $postid                     = $bizness_global_post->ID;
        $get_image                  = esc_url( wp_get_attachment_url( get_post_thumbnail_id($postid) ) );
        $teacher_label              = get_post_meta(get_the_ID(), 'teacher_label', true);
        $teacher_name               = get_post_meta(get_the_ID(), 'teacher_name', true);  
        $teacher_image_url          = get_post_meta(get_the_ID(), 'teacher_image_url', true);
        $teacher_designation        = get_post_meta(get_the_ID(), 'teacher_designation', true);
        $teacher_details            = get_post_meta(get_the_ID(), 'teacher_details', true);  
        $teacher_fb_url             = get_post_meta(get_the_ID(), 'teacher_fb_url', true);
        $teacher_twitter_url        = get_post_meta(get_the_ID(), 'teacher_twitter_url', true);
        $teacher_dribbble_url       = get_post_meta(get_the_ID(), 'teacher_dribbble_url', true);               
      ?>    
        <?php if( !empty($get_image) ):?>    
        <img src="<?php echo esc_url($get_image); ?>" alt="" class=" border_radius img-responsive bottom15">
        <?php endif; ?>
        <div class="detail_course">
          <?php if( !empty($teacher_label) && !empty($teacher_name) ): ?>
          <div class="info_label">
            <span class="icony"><i class="icon-users3"></i></span>
            <p><?php echo esc_attr($teacher_label); ?></p>
            <h5><?php echo esc_attr($teacher_name); ?></h5>
          </div>
          <?php endif; ?>
          <div class="info_label">
            <span class="icony"><i class="icon-users3"></i></span>
            <p><?php esc_html_e('Category', 'bizness'); ?></p>
            <?php 
              foreach( get_the_category(get_the_ID()) as $cat ){
            ?>
            <h5><?php echo esc_attr($cat->name); ?></h5>
            <?php } ?>
          </div>
          <div class="info_label hidden-xs"></div>
        </div>
        <h3 class="top30 bottom20"><?php the_title(); ?></h3>
        <?php the_content(); ?>

        <div class="bottom15"></div>
        <div class="profile_bg heading_space">
          <h3 class="bottom20"><?php esc_html_e('About', 'bizness'); ?> <?php echo esc_attr($teacher_label); ?></h3>
          <div class="profile">
            <?php if( !empty($teacher_image_url) ): ?>
            <div class="p_pic"><img src="<?php echo esc_url($teacher_image_url); ?>" alt=""></div>
            <?php endif; ?>
            <div class="profile_text">
              <h5><strong><?php echo esc_attr($teacher_name); ?></strong>  -  <span><?php echo esc_attr($teacher_designation); ?></span></h5>
              <p><?php echo esc_attr($teacher_details); ?></p>
              <ul class="social_icon black top20">
                <?php if( !empty($teacher_fb_url) ): ?>
                <li><a href="<?php echo esc_url($teacher_fb_url); ?>" class="facebook"><i class="fa fa-facebook"></i></a></li>
                <?php endif; ?>
                 <?php if( !empty($teacher_twitter_url) ): ?>
                <li><a href="<?php echo esc_url($teacher_twitter_url); ?>" class="twitter"><i class="icon-twitter4"></i></a></li>
                <?php endif; ?>
                 <?php if( !empty($teacher_dribbble_url) ): ?>
                <li><a href="<?php echo esc_url($teacher_dribbble_url); ?>" class="dribble"><i class="icon-dribbble5"></i></a></li>
                <?php endif; ?>
              </ul>
            </div>
          </div>
        </div>
        <?php endwhile; endif; ?>

        <?php comments_template( '', true ); ?> 
      </div>
      <!--Sidebar-->
      <?php get_sidebar(); ?>
    <!--Sidebar end-->         
    </div>
  </div>
</section>
<!-- Services -->


<?php get_footer(); ?>