<?php
/**
 * Template Name: Service
 */
get_header(); 
$bizness_page_heading              = get_post_meta(get_the_ID(), 'bizness_page_heading', true);
$bizness_page_subheading           = get_post_meta(get_the_ID(), 'bizness_page_subheading', true);
$bizness_page_bg                   = get_post_meta(get_the_ID(), 'bizness_page_bg', true);
?>

<!--Page Header-->
<section class="page_header padding-top" <?php if( !empty($bizness_page_bg)){ ?>style="background: url('<?php echo esc_url( $bizness_page_bg ); ?>');" <?php } else if(bizness_get_option('bizness_page_header_img') != ''){ ?>style="background: url('<?php echo esc_url( bizness_get_option('bizness_page_header_img') ); ?>');" <?php } else { ?>style="background: url('<?php echo esc_url( get_template_directory_uri() ).'/images/'; ?>page-tittle.jpg');"<?php } ?>>
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <?php if( !empty($bizness_page_heading) ): ?>
        <h1><?php echo esc_attr($bizness_page_heading); ?></h1>
        <?php else: ?>
          <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
        <h1><?php ucwords(the_title()); ?></h1>
          <?php endwhile; endif; ?>
        <?php endif; ?>  

        <?php if( !empty($bizness_page_subheading) ): ?>
        <p><?php echo esc_attr($bizness_page_subheading); ?></p>
        <?php else: ?>
        <p><?php bloginfo('description'); ?></p>
        <?php endif; ?>
        <div class="page_nav">
        <?php if (function_exists('bizness_wordpress_breadcrumbs')) bizness_wordpress_breadcrumbs(); ?>
        </div>      
      </div>
    </div>
  </div>
</section>

<!-- services -->
<section id="course_all" class="padding-bottom">
  <div class="container">
    <div class="row">
    <?php 
      $data_delay = 3;
      $allowed_html_array = array(
        'a' => array(),
        'span' => array(),
      );          
      if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
      elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
      else { $paged = 1; }
      $args = array('post_type'=> 'bizness-service','order'=> 'DESC', 'paged' => $paged, 'orderby' => 'post_date' );
      query_posts($args);    
    ?>
    <?php if (have_posts()) :  while (have_posts()) : the_post(); 
      $bizness_global_post       = bizness_get_global_post();
      $postid                 = $bizness_global_post->ID;
      $get_image              = esc_url( wp_get_attachment_url( get_post_thumbnail_id($postid) ) );  

      if( $data_delay > 11 ):
        $data_delay = 3;
      endif;                
    ?>      
      <div class="col-sm-6 col-md-4 equalheight">
        <div class="course margin_top wow fadeIn" data-wow-delay="<?php echo esc_attr($data_delay); ?>00ms">
          <?php if ( has_post_thumbnail() ): ?>
          <div class="image bottom25">
            <img src="<?php echo esc_url( $get_image ); ?>" alt="" class="border_radius">
          </div>
          <?php endif; ?>
          <h3 class="bottom10"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
          <p class="bottom20"><?php echo bizness_getexcerpt(111); ?></p>
          <a class="btn_common yellow border_radius" href="<?php the_permalink(); ?>"><?php esc_html_e('view details', 'bizness'); ?></a>
        </div>
      </div>
    <?php endwhile; endif; ?>
    </div>
  </div>
</section>
<!-- services ends -->


<?php get_footer(); ?>